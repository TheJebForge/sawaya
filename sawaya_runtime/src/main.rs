use flexi_logger::{Logger, LogSpecification, colored_opt_format, FileSpec, Criterion, Age, Naming, Cleanup, WriteMode, Duplicate, LevelFilter};
use sawaya_core::serenity::{async_trait, Client};
use sawaya_core::config::{SensitiveConfig, Config};
use sawaya_core::serenity::prelude::{EventHandler, Context};
use sawaya_core::serenity::model::prelude::{Message, Ready, Activity, VoiceState};
use sawaya_core::db::DB;
use std::sync::Arc;
use std::time::{SystemTime, UNIX_EPOCH};
use sawaya_core::lang::{load_lang_files};
use sawaya_core::get_modules;
use sawaya_core::processor::process_commands;
use sawaya_core::socket::launch_websocket;
use flexi_logger::writers::{FileLogWriter, FlWriteMode};
use sawaya_core::events::call_event;
use sawaya_core::events::voice_channel::{UserJoinedVC, UserLeftVC, UserMovedVC};
use sawaya_core::serenity::model::id::GuildId;

struct Handler {
    db: Arc<DB>,
    config: Arc<Config>,
    sensitive: Arc<SensitiveConfig>,
}

#[async_trait]
impl EventHandler for Handler {
    async fn message(&self, ctx: Context, message: Message) {
        process_commands(Arc::new(ctx), Arc::new(message), self.db.clone(), self.config.clone(), self.sensitive.clone()).await;
    }

    async fn ready(&self, ctx: Context, _data_about_bot: Ready) {
        log::info!("Client started, triggering init for modules");

        let ctx = Arc::new(ctx);

        // Launching web server
        launch_websocket(self.db.clone(), ctx.clone()).await;

        for module in get_modules().values() {
            module.init(ctx.clone(), self.db.clone());
        }

        log::info!("Initializing done");
        ctx.set_activity(Activity::watching("over the world")).await;
    }

    async fn voice_state_update(&self, ctx: Context, guild: Option<GuildId>, old: Option<VoiceState>, new: VoiceState) {
        let guild_name = if let Some(guild) = guild {
            guild.name(&ctx).await
        } else {
            None
        };

        let guild_id = if let Some(guild) = guild {
            Some(guild.0)
        } else {
            None
        };

        // Checking if user joined
        if old.is_none() {
            if let Ok(user) = new.user_id.to_user(&ctx).await {
                let user_name = format!("{}#{:04}", user.name, user.discriminator);

                let event = UserJoinedVC {
                    guild: guild_name,
                    guild_id,
                    channel: if let Some(channel) = new.channel_id {
                        channel.name(&ctx).await
                    } else {
                        None
                    },
                    channel_id: if let Some(channel) = new.channel_id {
                        Some(channel.0)
                    } else {
                        None
                    },
                    user: user_name.to_string(),
                    display_name: if let Some(member) = new.member {
                        member.nick.unwrap_or(user_name.to_string())
                    } else {
                        user_name.to_string()
                    },
                    user_id: user.id.0,
                    time: SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_secs()
                };

                call_event(Box::new(event));
            }
        } else if let Some(old) = old {
            // Checking if user left
            if new.channel_id.is_none() {
                if let Ok(user) = old.user_id.to_user(&ctx).await {
                    let user_name = format!("{}#{:04}", user.name, user.discriminator);

                    let event = UserLeftVC {
                        guild: guild_name,
                        guild_id,
                        channel: if let Some(channel) = old.channel_id {
                            channel.name(&ctx).await
                        } else {
                            None
                        },
                        channel_id: if let Some(channel) = old.channel_id {
                            Some(channel.0)
                        } else {
                            None
                        },
                        user: user_name.to_string(),
                        display_name: if let Some(member) = old.member {
                            member.nick.unwrap_or(user_name.to_string())
                        } else {
                            user_name.to_string()
                        },
                        user_id: user.id.0,
                        time: SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_secs()
                    };

                    call_event(Box::new(event));
                }
            } else {
                // Then user moved?
                if old.channel_id != new.channel_id {
                    if let Ok(user) = old.user_id.to_user(&ctx).await {
                        let user_name = format!("{}#{:04}", user.name, user.discriminator);

                        let event = UserMovedVC {
                            guild: guild_name,
                            guild_id,
                            from_channel: if let Some(channel) = old.channel_id {
                                channel.name(&ctx).await
                            } else {
                                None
                            },
                            from_channel_id: if let Some(channel) = old.channel_id {
                                Some(channel.0)
                            } else {
                                None
                            },
                            to_channel: if let Some(channel) = new.channel_id {
                                channel.name(&ctx).await
                            } else {
                                None
                            },
                            to_channel_id: if let Some(channel) = new.channel_id {
                                Some(channel.0)
                            } else {
                                None
                            },
                            user: user_name.to_string(),
                            display_name: if let Some(member) = old.member {
                                member.nick.unwrap_or(user_name.to_string())
                            } else {
                                user_name.to_string()
                            },
                            user_id: user.id.0,
                            time: SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_secs()
                        };

                        call_event(Box::new(event));
                    }
                }
            }
        }
    }
}

pub fn get_event_writer() -> Box<FileLogWriter> {
    let writer = FileLogWriter::builder(
        FileSpec::default()
            .directory("log")
            .basename("sawaya_event_log")
            .suffix("log")
    ).format(
        |w, now, record| {
            write!(w, "{} {}", now.now().timestamp(), record.args())
        }
    ).rotate(
        Criterion::Age(Age::Day),
        Naming::Timestamps,
        Cleanup::KeepCompressedFiles(999999)
    ).write_mode(
        FlWriteMode::DontBuffer
    ).try_build().unwrap();

    Box::new(writer)
}

#[tokio::main]
async fn main() {
    // Initializing logger
    let mut builder = LogSpecification::builder();
    builder
        .default(LevelFilter::Off)
        .module("sawaya", LevelFilter::Trace)
        .module("rocket", LevelFilter::Trace);

    Logger::with(builder.build())
        .add_writer("event_log", get_event_writer())
        .log_to_file(
            FileSpec::default()
                .directory("log")
                .basename("sawaya_log")
                .suffix("log")
        )
        .rotate(
            Criterion::Age(Age::Day),
            Naming::Timestamps,
            Cleanup::KeepLogAndCompressedFiles(7, 90)
        )
        .write_mode(WriteMode::BufferAndFlush)
        .duplicate_to_stdout(Duplicate::All)
        .format(colored_opt_format)
        .start().unwrap();

    log::info!("Initializing on docker with automatic deployment...");
    log::info!("Initialized logger");

    // Loading language files
    load_lang_files();

    // Connecting to database
    let db = DB::connect().await;

    // Modules
    sawaya_test::init();
    sawaya_config::init();
    sawaya_debug::init();
    sawaya_dailytop::init();
    sawaya_mover::init();
    sawaya_custom_commands::init(db.clone()).await;

    // Starting discord client
    let token = SensitiveConfig::get().token;

    let mut client = Client::builder(&token)
        .event_handler(Handler {
            db,
            config: Arc::new(Config::get()),
            sensitive: Arc::new(SensitiveConfig::get())
        })
        .await.expect("Error creating client");

    if let Err(why) = client.start().await {
        log::error!("Client error: {:?}", why);
    }
}
