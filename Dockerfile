FROM rust:1.56 as builder
WORKDIR /sawaya
COPY . .
RUN cargo build --release
RUN cp /sawaya/target/release/sawaya_runtime /sawaya/sawaya
RUN rm -rf /sawaya/target
EXPOSE 9001
CMD ["./sawaya"]