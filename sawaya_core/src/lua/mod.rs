pub mod bot;
mod print;

use mlua::{Lua};
use crate::command::data::CommandData;
use std::sync::{Arc, Mutex};
use crate::lua::bot::{register_bot_api};
use crate::lua::print::register_loggers;

pub struct LuaState {
    pub log: Arc<Mutex<String>>,
    pub lua: Arc<Lua>
}

impl LuaState {
    pub fn execute(&self, code: String) -> mlua::Result<()> {
        self.lua.load(&code).exec()
    }

    pub fn get_log(&self) -> String {
        self.log.lock().unwrap().to_string()
    }
}

pub fn create_lua_state(data: Arc<CommandData>) -> LuaState {
    let lua = Arc::new(Lua::new());
    let log = Arc::new(Mutex::new(String::new()));

    register_bot_api(lua.clone(), data.clone());
    register_loggers(lua.clone(), log.clone());

    LuaState {
        log,
        lua
    }
}