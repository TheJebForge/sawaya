use std::fmt::{Display, Formatter};
use std::ops::Deref;
use std::sync::Arc;
use mlua::{MetaMethod, UserData, UserDataFields, UserDataMethods};
use crate::lua::bot::member::LuaMember;
use crate::lua::bot::role::LuaRole;
use crate::serenity::client::Context;
use crate::serenity::model::guild::Guild;
use crate::block;
use crate::lua::bot::voice_states::LuaVoiceState;

pub struct LuaGuild {
    pub ctx: Arc<Context>,
    pub guild: Guild
}

impl LuaGuild {
    pub fn member(&self, id: u64) -> Option<LuaMember> {
        if let Ok(member) = block!(self.guild.id.member(self.ctx.http.deref(), id)) {
            Some(LuaMember {
                ctx: self.ctx.clone(),
                member
            })
        } else {
            None
        }
    }

    pub fn members(&self) -> Vec<LuaMember> {
        self.guild.members.values().map(|x| LuaMember { ctx: self.ctx.clone(), member: x.clone() }).collect()
    }

    pub fn roles(&self) -> Vec<LuaRole> {
        self.guild.roles.values().map(|x| LuaRole { ctx: self.ctx.clone(), role: x.clone()}).collect()
    }

    pub fn voice_states(&self) -> Vec<LuaVoiceState> {
        self.guild.voice_states.values().map(|x| LuaVoiceState { ctx: self.ctx.clone(), voice_state: x.clone() }).collect()
    }
}

impl Display for LuaGuild {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "Guild {{ id = \"{}\", name = \"{}\", members = {{...}}, roles = {{...}}, voice_states = {{...}} }}", self.guild.id.0, self.guild.name)
    }
}

impl UserData for LuaGuild {
    fn add_fields<'lua, F: UserDataFields<'lua, Self>>(fields: &mut F) {
        fields.add_field_method_get("id", |_, this| {
            Ok(this.guild.id.0.to_string())
        });

        fields.add_field_method_get("name", |_, this| {
            Ok(this.guild.name.to_string())
        });

        fields.add_field_method_get("members", |_, this| {
            Ok(this.members())
        });

        fields.add_field_method_get("roles", |_, this| {
            Ok(this.roles())
        });

        fields.add_field_method_get("voice_states", |_, this| {
            Ok(this.voice_states())
        });
    }

    fn add_methods<'lua, M: UserDataMethods<'lua, Self>>(methods: &mut M) {
        methods.add_method("get_member", |_, this, a: String| {
            if let Ok(a) = a.parse::<u64>() {
                Ok((Some(this.member(a)), None))
            } else {
                Ok((None, Some("Couldn't parse ID".to_string())))
            }
        });

        methods.add_meta_method(MetaMethod::ToString, |_, this, _: ()| {
            Ok(this.to_string())
        });
    }
}

