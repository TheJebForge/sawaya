use serenity::model::user::User;
use std::sync::Arc;
use serenity::client::Context;
use mlua::{UserData, UserDataFields, UserDataMethods, MetaMethod};
use std::fmt::{Display, Formatter};

#[allow(dead_code)]
pub struct LuaUser {
    pub ctx: Arc<Context>,
    pub user: User
}

impl Display for LuaUser {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "User {{ id = \"{}\", name = \"{}\", discriminator = \"{:0>4}\", bot = {} }}", self.user.id.0, self.user.name, self.user.discriminator, self.user.bot)
    }
}

impl UserData for LuaUser {
    fn add_fields<'lua, F: UserDataFields<'lua, Self>>(fields: &mut F) {
        fields.add_field_method_get("id", |_, this| {
            Ok(this.user.id.0.to_string())
        });

        fields.add_field_method_get("name", |_, this| {
            Ok(this.user.name.to_string())
        });

        fields.add_field_method_get("discriminator", |_, this| {
            Ok(format!("{:0>4}", this.user.discriminator))
        });

        fields.add_field_method_get("bot", |_, this| {
            Ok(this.user.bot)
        })
    }

    fn add_methods<'lua, M: UserDataMethods<'lua, Self>>(methods: &mut M) {
        methods.add_meta_method(MetaMethod::ToString, |_, this, _: ()| {
            Ok(this.to_string())
        });
    }
}