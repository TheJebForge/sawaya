use crate::block;
use mlua::{Table, UserData, UserDataFields, UserDataMethods, MetaMethod};
use crate::command::data::CommandData;
use std::sync::Arc;
use serenity::client::Context;
use serenity::model::channel::Message;
use crate::lua::bot::user::LuaUser;
use std::fmt::{Display, Formatter};
use std::ops::Deref;
use crate::lua::bot::channel::LuaChannel;
use crate::lua::bot::guild::LuaGuild;
use crate::lua::bot::member::LuaMember;

#[allow(dead_code)]
pub struct LuaMessage {
    pub ctx: Arc<Context>,
    pub msg: Arc<Message>
}

impl LuaMessage {
    pub fn user(&self) -> LuaUser {
        LuaUser {
            ctx: self.ctx.clone(),
            user: self.msg.author.clone()
        }
    }

    pub fn channel(&self) -> LuaChannel {
        LuaChannel {
            ctx: self.ctx.clone(),
            channel: block!(self.msg.channel_id.to_channel(self.ctx.deref())).unwrap()
        }
    }

    pub fn guild(&self) -> Option<LuaGuild> {
        if let Some(guild) = block!(self.msg.guild(self.ctx.deref())) {
            Some(LuaGuild {
                ctx: self.ctx.clone(),
                guild
            })
        } else {
            None
        }
    }

    pub fn member(&self) -> Option<LuaMember> {
        if let Some(guild) = self.guild() {
            guild.member(self.msg.author.id.0)
        } else {
            None
        }
    }
}

impl Display for LuaMessage {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "Message {{ content = \"{}\", author = {}, channel = {} }}", self.msg.content, self.user().to_string(), self.channel().to_string())
    }
}

impl UserData for LuaMessage {
    fn add_fields<'lua, F: UserDataFields<'lua, Self>>(fields: &mut F) {
        fields.add_field_method_get("content", |_, this| {
            Ok(this.msg.content.to_string())
        });

        fields.add_field_method_get("author", |_, this| {
           Ok(this.user())
        });
        
        fields.add_field_method_get("channel", |_, this| {
            Ok(this.channel())
        });

        fields.add_field_method_get("member", |_, this| {
            Ok(this.member())
        });

        fields.add_field_method_get("guild", |_, this| {
            Ok(this.guild())
        });
    }

    fn add_methods<'lua, M: UserDataMethods<'lua, Self>>(methods: &mut M) {
        methods.add_method("delete", |_, this, _: ()| {
            block!(this.msg.delete(this.ctx.deref())).ok();
            Ok(())
        });

        methods.add_meta_method(MetaMethod::ToString, |_, this, _: ()| {
            Ok(this.to_string())
        });
    }
}



pub fn register_message(bot_table: &Table, data: Arc<CommandData>) {
    bot_table.set("msg", LuaMessage { ctx: data.ctx.clone(), msg: data.message.clone() }).ok();
}