use std::ops::Deref;
use std::sync::Arc;
use crate::command::data::CommandData;
use crate::lua::bot::message::register_message;
use mlua::{Lua, Table, Value};
use serenity::model::id::{ChannelId, GuildId, UserId};
use crate::embed::EmbedData;
use crate::lua::bot::user::LuaUser;
use crate::block;
use crate::lua::bot::channel::LuaChannel;
use crate::lua::bot::guild::LuaGuild;

pub mod message;
pub mod user;
pub mod channel;
pub mod member;
pub mod role;
pub mod guild;
pub mod argument;
pub mod voice_states;

pub fn register_bot_api(lua: Arc<Lua>, data: Arc<CommandData>) {
    let val = lua.create_table().unwrap();

    register_message(&val, data.clone());
    fetch_user(&val, lua.clone(), data.clone());
    fetch_channel(&val, lua.clone(), data.clone());
    fetch_guild(&val, lua.clone(), data.clone());

    

    lua.globals().set("bot", val).unwrap()
}

pub fn fetch_user(table: &Table, lua: Arc<Lua>, data: Arc<CommandData>) {
    table.set("fetch_user", lua.create_function(move |_, a: String| {
        if let Ok(a) = a.parse::<u64>() {
            let user = block!(UserId(a).to_user(data.ctx.deref()));

            match user {
                Ok(user) => {
                    Ok((Some(LuaUser { ctx: data.ctx.clone(), user }), None))
                }

                Err(err) => {
                    Ok((None, Some(err.to_string())))
                }
            }
        } else {
            Ok((None, Some("Couldn't parse ID".to_string())))
        }
    }).unwrap()).ok();
}

pub fn fetch_channel(table: &Table, lua: Arc<Lua>, data: Arc<CommandData>) {
    table.set("fetch_channel", lua.create_function(move |_, a: String| {
        if let Ok(a) = a.parse::<u64>() {
            let channel = block!(ChannelId(a).to_channel(data.ctx.deref()));

            match channel {
                Ok(channel) => {
                    Ok((Some(LuaChannel { ctx: data.ctx.clone(), channel }), None))
                }

                Err(err) => {
                    Ok((None, Some(err.to_string())))
                }
            }
        } else {
            Ok((None, Some("Couldn't parse ID".to_string())))
        }
    }).unwrap()).ok();
}

pub fn fetch_guild(table: &Table, lua: Arc<Lua>, data: Arc<CommandData>) {
    table.set("fetch_guild", lua.create_function(move |_, a: String| {
        if let Ok(a) = a.parse::<u64>() {
            let guild = block!(GuildId(a).to_guild_cached(data.ctx.deref()));

            match guild {
                Some(guild) => {
                    Ok((Some(LuaGuild { ctx: data.ctx.clone(), guild }), None))
                }

                None => {
                    Ok((None, Some("Not found".to_string())))
                }
            }
        } else {
            Ok((None, Some("Couldn't parse ID".to_string())))
        }
    }).unwrap()).ok();
}

pub fn break_down_to_embed(value: &Value) -> EmbedData {
    if let Value::Table(table) = value {
        let mut embed = EmbedData::new();

        if let Ok(v) = table.get::<&str, String>("title") {
            embed.title = v;
        }

        if let Ok(v) = table.get::<&str, String>("desc") {
            embed.desc = Some(v);
        }

        if let Ok(v) = table.get::<&str, u32>("color") {
            embed.color = v;
        }

        embed
    } else {
        EmbedData::new()
    }
}