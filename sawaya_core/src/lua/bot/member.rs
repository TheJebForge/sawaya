use std::fmt::{Display, Formatter};
use std::ops::Deref;
use std::sync::Arc;
use mlua::{MetaMethod, UserData, UserDataFields, UserDataMethods};
use serenity::model::guild::Member;
use crate::lua::bot::role::LuaRole;
use crate::serenity::client::Context;
use crate::block;
use crate::lua::bot::user::LuaUser;

#[derive(Clone)]
pub struct LuaMember {
    pub ctx: Arc<Context>,
    pub member: Member
}

impl LuaMember {
    pub fn roles(&self) -> Vec<LuaRole> {
        if let Some(role) = block!(self.member.roles(self.ctx.deref())) {
            role.into_iter().map(|x| LuaRole { ctx: self.ctx.clone(), role: x}).collect()
        } else {
            vec![]
        }
    }

    pub fn user(&self) -> LuaUser {
        LuaUser {
            ctx: self.ctx.clone(),
            user: self.member.user.clone()
        }
    }

    pub fn display_name(&self) -> String {
        self.member.display_name().to_string()
    }
}

impl Display for LuaMember {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "Member {{ display_name = \"{}\", user = {}, roles = {{...}} }}", self.display_name(), self.user())
    }
}

impl UserData for LuaMember {
    fn add_fields<'lua, F: UserDataFields<'lua, Self>>(fields: &mut F) {
        fields.add_field_method_get("display_name", |_, this| {
            Ok(this.display_name())
        });

        fields.add_field_method_get("user", |_, this| {
            Ok(this.user())
        });

        fields.add_field_method_get("roles", |_, this| {
            Ok(this.roles())
        });
    }

    fn add_methods<'lua, M: UserDataMethods<'lua, Self>>(methods: &mut M) {
        methods.add_method("contains_role", |_, this, id: String| {
            if let Ok(id) = id.parse::<u64>() {
                Ok((Some(this.member.roles.contains(&id.into())), None))
            } else {
                Ok((None, Some("Couldn't parse ID".to_string())))
            }
        });

        methods.add_method_mut("add_role", |_, this, id: String| {
            if let Ok(id) = id.parse::<u64>() {
                if let Ok(_) = block!(this.member.add_role(this.ctx.deref(), id)) {
                    Ok((Some(this.clone()), None))
                } else {
                    Ok((None, None))
                }
            } else {
                Ok((None, Some("Couldn't parse ID".to_string())))
            }
        });

        methods.add_method_mut("remove_role", |_, this, id: String| {
            if let Ok(id) = id.parse::<u64>() {
                if let Ok(_) = block!(this.member.remove_role(this.ctx.deref(), id)) {
                    Ok((Some(this.clone()), None))
                } else {
                    Ok((None, None))
                }
            } else {
                Ok((None, Some("Couldn't parse ID".to_string())))
            }
        });

        methods.add_meta_method(MetaMethod::ToString, |_, this, _: ()| {
            Ok(this.to_string())
        });
    }
}