use std::fmt::{Display, Formatter};
use std::sync::Arc;
use mlua::{MetaMethod, UserData, UserDataFields, UserDataMethods};

use crate::serenity::client::Context;
use crate::serenity::model::guild::Role;

pub struct LuaRole {
    pub ctx: Arc<Context>,
    pub role: Role
}

impl Display for LuaRole {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "Role {{ id = \'{}\", name = \"{}\" }}", self.role.id.0, self.role.name)
    }
}

impl UserData for LuaRole {
    fn add_fields<'lua, F: UserDataFields<'lua, Self>>(fields: &mut F) {
        fields.add_field_method_get("id", |_, this| {
            Ok(this.role.id.0.to_string())
        });

        fields.add_field_method_get("name", |_, this| {
            Ok(this.role.name.to_string())
        });
    }

    fn add_methods<'lua, M: UserDataMethods<'lua, Self>>(methods: &mut M) {
        methods.add_meta_method(MetaMethod::ToString, |_, this, _: ()| {
            Ok(this.to_string())
        });
    }
}