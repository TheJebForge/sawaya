use std::fmt::{Display, Formatter};
use std::ops::Deref;
use std::sync::Arc;
use mlua::{MetaMethod, UserData, UserDataFields, UserDataMethods, Value};
use serenity::client::Context;
use serenity::model::channel::Channel;
use crate::block;
use crate::lua::bot::break_down_to_embed;
use crate::lua::bot::message::LuaMessage;
use crate::processor::embed::process_embed;

pub struct LuaChannel {
    pub ctx: Arc<Context>,
    pub channel: Channel
}

impl LuaChannel {
    pub fn channel_type(&self) -> String {
        match self.channel {
            Channel::Guild(_) => "guild".to_string(),
            Channel::Private(_) => "private".to_string(),
            Channel::Category(_) => "category".to_string(),
            _ => "unknown".to_string()
        }
    }

    pub fn name(&self) -> String {
        match &self.channel {
            Channel::Guild(guild) => guild.name.to_string(),
            Channel::Private(private) => private.name(),
            Channel::Category(category) => category.name.to_string(),
            _ => "unknown".to_string()
        }
    }
}

impl Display for LuaChannel {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "Channel {{ id = \"{}\", type = \"{}\", name = \"{}\" }}", self.channel.id().0, self.channel_type(), self.name())
    }
}

impl UserData for LuaChannel {
    fn add_fields<'lua, F: UserDataFields<'lua, Self>>(fields: &mut F) {
        fields.add_field_method_get("id", |_, this| {
            Ok(this.channel.id().0.to_string())
        });

        fields.add_field_method_get("type", |_, this| {
            Ok(this.channel_type())
        });

        fields.add_field_method_get("name", |_, this| {
           Ok(this.name())
        });
    }

    fn add_methods<'lua, M: UserDataMethods<'lua, Self>>(methods: &mut M) {
        methods.add_meta_method(MetaMethod::ToString, |_, this, _: ()| {
            Ok(this.to_string())
        });

        methods.add_method("send_text", |_, this, a: String| {
            match &this.channel {
                Channel::Guild(guild) => {
                    let mes = block!(guild.send_message(this.ctx.deref(), |m| {
                        m.content(a)
                    }));

                    if let Ok(mes) = mes {
                        Ok(Some(LuaMessage { ctx: this.ctx.clone(), msg: Arc::new(mes) }))
                    } else {
                        Ok(None)
                    }
                }

                Channel::Private(private) => {
                    let mes = block!(private.send_message(this.ctx.deref(), |m| {
                        m.content(a)
                    }));

                    if let Ok(mes) = mes {
                        Ok(Some(LuaMessage { ctx: this.ctx.clone(), msg: Arc::new(mes) }))
                    } else {
                        Ok(None)
                    }
                }

                _ => Ok(None)
            }
        });

        methods.add_method("send_embed", |_, this, a: Value| {
            match &this.channel {
                Channel::Guild(guild) => {
                    let embed = break_down_to_embed(&a);

                    let mes = block!(guild.send_message(this.ctx.deref(), |m| {
                        m.set_embed(process_embed(&embed))
                    }));

                    if let Ok(mes) = mes {
                        Ok(Some(LuaMessage { ctx: this.ctx.clone(), msg: Arc::new(mes) }))
                    } else {
                        Ok(None)
                    }
                }

                Channel::Private(private) => {
                    let embed = break_down_to_embed(&a);

                    let mes = block!(private.send_message(this.ctx.deref(), |m| {
                        m.set_embed(process_embed(&embed))
                    }));

                    if let Ok(mes) = mes {
                        Ok(Some(LuaMessage { ctx: this.ctx.clone(), msg: Arc::new(mes) }))
                    } else {
                        Ok(None)
                    }
                }

                _ => Ok(None)
            }
        });
    }
}