use std::fmt::{Display, Formatter};
use std::sync::Arc;
use mlua::{MetaMethod, UserData, UserDataFields, UserDataMethods};
use serenity::model::id::ChannelId;
use serenity::model::prelude::{VoiceState};
use serenity::prelude::Context;

#[allow(dead_code)]
pub struct LuaVoiceState {
    pub ctx: Arc<Context>,
    pub voice_state: VoiceState
}

impl Display for LuaVoiceState {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "Voice State {{ user_id = \"{}\", channel_id = \"{}\" }}",
               self.voice_state.user_id.0, self.voice_state.channel_id.unwrap_or(ChannelId(0)).0)
    }
}

impl UserData for LuaVoiceState {
    fn add_fields<'lua, F: UserDataFields<'lua, Self>>(fields: &mut F) {
        fields.add_field_method_get("user_id", |_, this| {
            Ok(this.voice_state.user_id.0.to_string())
        });

        fields.add_field_method_get("channel_id", |_, this| {
            Ok(this.voice_state.channel_id.unwrap_or(ChannelId(0)).0.to_string())
        });
    }

    fn add_methods<'lua, M: UserDataMethods<'lua, Self>>(methods: &mut M) {
        methods.add_meta_method(MetaMethod::ToString, |_, this, _: ()| {
            Ok(this.to_string())
        });
    }
}