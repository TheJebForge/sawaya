use std::sync::Arc;
use mlua::{MetaMethod, UserData, UserDataFields, UserDataMethods};
use serenity::client::Context;
use crate::command::arguments::Argument;
use crate::lua::bot::user::LuaUser;

pub struct LuaArgument {
    ctx: Arc<Context>,
    argument: Argument
}

#[allow(dead_code)]
impl LuaArgument {
    pub fn from_argument(ctx: Arc<Context>, argument: Argument) -> LuaArgument {
        LuaArgument {
            ctx,
            argument
        }
    }
}

impl UserData for LuaArgument {
    fn add_fields<'lua, F: UserDataFields<'lua, Self>>(fields: &mut F) {
        fields.add_field_method_get("arg_type", |_, this| {
            Ok(this.argument.argument_type())
        });

        fields.add_field_method_get("string", |_, this| {
            Ok(this.argument.to_string())
        });

        fields.add_field_method_get("text", |_, this| {
            Ok(this.argument.to_text())
        });

        fields.add_field_method_get("integer", |_, this| {
            Ok(this.argument.to_int())
        });

        fields.add_field_method_get("float", |_, this| {
            Ok(this.argument.to_float())
        });

        fields.add_field_method_get("mention", |_, this| {
            if let Argument::Mention(user) = &this.argument {
                Ok(Some(LuaUser { ctx: this.ctx.clone(), user: user.clone() }))
            } else {
                Ok(None)
            }
        });
    }

    fn add_methods<'lua, M: UserDataMethods<'lua, Self>>(methods: &mut M) {
        methods.add_meta_method(MetaMethod::ToString, |_, this, _: ()| {
            let argument_type = this.argument.argument_type();

            Ok(match &this.argument {
                Argument::None => format!("{}", argument_type),
                Argument::Integer(int) => format!("{}({})", argument_type, int),
                Argument::Float(float) => format!("{}({})", argument_type, float),
                Argument::String(str) => format!("{}(\"{}\")", argument_type, str),
                Argument::Text(text) => format!("{}(\"{}\")", argument_type, text),
                Argument::Mention(user) => format!("{}(\"{}\")", argument_type, user.id.0),
            })
        });
    }
}