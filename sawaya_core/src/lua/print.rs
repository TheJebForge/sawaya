use mlua::{Lua, MultiValue, TableExt, Value};
use std::sync::{Arc, Mutex};

const WIDTH: usize = 4;

pub fn register_loggers(lua: Arc<Lua>, log: Arc<Mutex<String>>) {
    print(lua.clone(), log.clone());
}

pub fn print(lua: Arc<Lua>, log: Arc<Mutex<String>>) {
    let closure_lua = lua.clone();

    let func = lua.create_function(move |_, values: MultiValue| {
        let mut str = String::new();

        for value in values {
            let value_str = closure_lua.globals().call_function("tostring", value).unwrap();

            if let Value::String(value_str) = &value_str {
                let value_str = value_str.to_str().unwrap();

                str.push_str(&format!("{:<1$}  ", value_str, (value_str.len() / WIDTH + 1) * WIDTH));
            }

        }

        log.lock().unwrap().push_str(&format!("{}\n", str.trim()));

        Ok(())
    }).unwrap();

    lua.globals().set("print", func).unwrap();
}