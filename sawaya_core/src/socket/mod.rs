pub mod types;
pub mod db;
pub mod processor;

use std::borrow::Cow;
use std::ops::Add;
use std::sync::Arc;
use serenity::client::Context;
use std::thread;
use std::time::Duration;
use futures_util::{FutureExt, SinkExt, StreamExt};
use tokio::net::{TcpListener, TcpStream};
use tokio::runtime::Handle;
use tokio::time::Instant;
use tokio_tungstenite::tungstenite::handshake::server::{Request, Response, ErrorResponse};
use tokio_tungstenite::tungstenite::http::header::SEC_WEBSOCKET_PROTOCOL;
use tokio_tungstenite::tungstenite::Message;
use tokio_tungstenite::tungstenite::protocol::CloseFrame;
use tokio_tungstenite::tungstenite::protocol::frame::coding::CloseCode;
use crate::db::DB;
use crate::socket::processor::{process_packet, register_socket_request};
use crate::block;
use crate::config::Config;
use crate::events::bot_event_pool;
use crate::socket::types::guild_info::GuildInfoRequest;
use crate::socket::types::guild_modules::GuildModulesRequest;
use crate::socket::types::managed_guilds::ManagedGuildsRequest;
use crate::socket::types::WebSocketPacket;

fn register_base_requests() {
    register_socket_request(Box::new(ManagedGuildsRequest));
    register_socket_request(Box::new(GuildModulesRequest));
    register_socket_request(Box::new(GuildInfoRequest));
}

pub async fn launch_websocket(db: Arc<DB>, ctx: Arc<Context>) {
    register_base_requests();

    let handle = Handle::current();

    let config = Config::get();

    let try_server = TcpListener::bind(config.websocket_bind).await;
    let listener = try_server.expect("Failed to bind");

    log::info!("Starting websocket server");



    thread::spawn(move || {
        handle.clone().spawn(async move {
            let counter = Arc::new(());
            while let Ok((stream, _)) = listener.accept().await {
                let handle = handle.clone();
                let db = db.clone();
                let ctx= ctx.clone();
                let stream_counter = counter.clone();
                thread::spawn(move || {
                    handle.spawn(accept_connection(stream, db, ctx, stream_counter));
                });
            }
        });
    });
}

async fn accept_connection(stream: TcpStream, db: Arc<DB>, ctx: Arc<Context>, counter: Arc<()>) {
    let addr = stream.peer_addr().expect("connected streams should have a peer address");
    log::info!("Peer address: {}", addr);

    let mut auth_user_id = "".to_string();

    let ws_stream = tokio_tungstenite::accept_hdr_async(stream, |request: &Request, mut response: Response| {
        if let Some(value) = request.headers().get(SEC_WEBSOCKET_PROTOCOL) {
            if let Ok(value) = value.to_str(){
                if let Some(user_id) = block!(db.get_auth_key(value)) {
                    auth_user_id = user_id.to_string();
                    response.headers_mut().insert(SEC_WEBSOCKET_PROTOCOL, value.parse().unwrap());
                    Ok(response)
                } else {
                    Err(ErrorResponse::new(Some("Unauthorized".to_string())))
                }
            } else {
                Err(ErrorResponse::new(Some("Unauthorized".to_string())))
            }
        } else {
            Err(ErrorResponse::new(Some("Unauthorized".to_string())))
        }
    })
        .await;

    if let Ok(mut ws_stream) = ws_stream {
        log::info!("New WebSocket connection: {}; Total connections: {}", addr, Arc::strong_count(&counter) - 1);
        let mut span = Instant::now();
        let mut count = 0;

        let (_handle, pool) = bot_event_pool().await;

        let mut timeout = Instant::now().add(Duration::from_secs(40));

        loop {
            {
                let mut pool = pool.lock().await;
                while let Some(event) = pool.next() {
                    let response = WebSocketPacket {
                        requester: None,
                        ty: "bot_event".to_string(),
                        data: Some(event.to_map())
                    };

                    ws_stream.send(Message::Text(serde_json::to_string(&response).unwrap())).await.ok();
                }
            }

            let future = ws_stream.next();
            if let Some(msg) = future.now_or_never() {
                if let Some(msg) = msg {
                    if let Ok(msg) = msg {
                        if msg.is_text() {
                            if !(msg.to_string() == "heartbeat") {
                                count += 1;
                                ws_stream.send(process_packet(auth_user_id.to_string(), &msg, db.clone(), ctx.clone()).await).await.ok();

                                if (span.elapsed().as_secs() > 60) && (count > 0) {
                                    log::info!("Connection {} performed {} requests over 60 seconds. {:.2} requests per second.", addr, count, count as f64 / 60.0);
                                    count = 0;
                                    span = Instant::now();
                                }
                            }

                            timeout = Instant::now().add(Duration::from_secs(40));
                        }
                    }
                } else {
                    break;
                }
            }

            if timeout < Instant::now() {
                ws_stream.close(Some(CloseFrame {
                    code: CloseCode::Error,
                    reason: Cow::from("No heartbeat for last 40 seconds")
                })).await.ok();
                break;
            }
        }

        log::info!("WebSocket connection closed: {}", addr);
    } else {
        log::info!("Failed handshake for websocket connection");
    }
}