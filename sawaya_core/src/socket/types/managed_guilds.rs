use std::ops::Deref;
use std::sync::Arc;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use serenity::model::id::{ChannelId, GuildId};
use crate::db::DB;
use crate::serenity::client::Context;
use crate::socket::types::SocketRequest;
use crate::async_trait;
use crate::serenity::model::channel::Channel;

pub struct ManagedGuildsRequest;

#[derive(Serialize, Deserialize)]
pub struct ManagedGuildsResponse {
    pub guilds: Vec<ManagedGuild>,
}

#[derive(Serialize, Deserialize)]
pub struct ManagedGuild {
    pub display_name: String,
    pub logo_url: Option<String>,
    pub guild_id: String,
    pub modules: Vec<String>,
}

#[async_trait]
impl SocketRequest for ManagedGuildsRequest {
    fn request_type(&self) -> &'static str {
        "managed_guilds"
    }

    async fn execute(&self, _data: Value, user_id: String, db: Arc<DB>, ctx: Arc<Context>) -> Result<String, String> {
        let guilds = db.get_manager_guilds(&user_id).await;

        let mut managed_guilds = vec![];

        for guild in guilds {
            let id = guild.parse::<u64>().unwrap();
            let modules: Vec<String> = db.get_enabled_modules(&guild).await.iter().map(|x| x.codename().to_string()).collect();

            if let Some(guild) = GuildId(id).to_guild_cached(ctx.deref()).await {
                managed_guilds.push(ManagedGuild {
                    display_name: guild.name.to_string(),
                    logo_url: guild.icon_url(),
                    guild_id: guild.id.0.to_string(),
                    modules
                })
            } else if let Ok(channel) = ChannelId(id).to_channel(ctx.deref()).await {
                if let Channel::Private(channel) = channel {
                    managed_guilds.push(ManagedGuild {
                        display_name: channel.recipient.name.to_string(),
                        logo_url: channel.recipient.avatar_url(),
                        guild_id: channel.id.0.to_string(),
                        modules
                    })
                }
            }
        }

        Ok(serde_json::to_string(&ManagedGuildsResponse {
            guilds: managed_guilds
        }).unwrap())
    }
}