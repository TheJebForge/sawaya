pub mod managed_guilds;
pub mod guild_modules;
pub mod guild_info;

use std::collections::HashMap;
use std::sync::Arc;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use serenity::client::Context;
use crate::db::DB;
use crate::async_trait;

#[derive(Serialize, Deserialize)]
pub struct WebSocketPacket {
    pub requester: Option<String>,
    pub ty: String,
    pub data: Option<HashMap<String, Value>>,
}

#[derive(Serialize, Deserialize)]
pub struct ErrorResponse {
    pub error: String,
}

pub type BoxedSocketRequest = Box<dyn SocketRequest + Send + Sync>;

#[async_trait]
pub trait SocketRequest {
    fn request_type(&self) -> &'static str;
    async fn execute(&self, data: Value, user_id: String, db: Arc<DB>, ctx: Arc<Context>) -> Result<String, String>;
}