use std::ops::Deref;
use std::sync::Arc;
use crate::async_trait;
use crate::trn;
use serde::{Serialize, Deserialize};
use serde_json::Value;
use serenity::model::id::UserId;
use crate::db::DB;
use crate::lang::get_lang;
use crate::serenity::client::Context;
use crate::socket::types::SocketRequest;

pub struct GuildInfoRequest;

#[derive(Serialize, Deserialize)]
pub struct GuildInfoRequestData {
    pub guild_id: String,
}

#[derive(Serialize, Deserialize)]
pub struct GuildInfoResponse {
    pub language: String,
    pub modules: Vec<GuildInfoModule>,
    pub managers: Vec<GuildInfoManager>
}

#[derive(Serialize, Deserialize)]
pub struct GuildInfoModule {
    pub codename: String,
    pub display_name: String,
    pub color: String,
}

#[derive(Serialize, Deserialize)]
pub struct GuildInfoManager {
    pub user_id: String,
    pub display_name: String,
    pub avatar_url: String,
}

#[async_trait]
impl SocketRequest for GuildInfoRequest {
    fn request_type(&self) -> &'static str {
        "guild_info"
    }

    async fn execute(&self, data: Value, _user_id: String, db: Arc<DB>, ctx: Arc<Context>) -> Result<String, String> {
        let data = serde_json::from_value::<GuildInfoRequestData>(data);

        match data {
            Ok(data) => {
                let lang = get_lang("english");

                let guild_data = db.get_guild(&data.guild_id).await;
                
                if let Some(guild_data) = guild_data {
                    let mut response = GuildInfoResponse {
                        language: guild_data.language.to_string(),
                        modules: vec![],
                        managers: vec![]
                    };
                    
                    let modules = db.get_enabled_modules(&data.guild_id).await;
                    for module in modules {
                        response.modules.push(GuildInfoModule {
                            codename: module.codename().to_string(),
                            display_name: trn!(lang, &format!("modules.{}.name", module.codename())),
                            color: format!("#{:X}", module.about(lang.clone()).color),
                        })
                    }

                    let managers = db.get_managers(&data.guild_id).await;
                    for manager in managers {
                        if let Ok(user) = UserId(manager.parse().unwrap()).to_user(ctx.deref()).await {
                            response.managers.push(GuildInfoManager {
                                user_id: manager.to_string(),
                                display_name: format!("{}#{:0>4}", user.name, user.discriminator),
                                avatar_url: user.avatar_url().unwrap_or_else(|| user.default_avatar_url())
                            })
                        }
                    }
                    
                    Ok(serde_json::to_string(&response).unwrap())
                } else {
                    Err("Guild not found".to_string())
                }
            }

            Err(err) => {
                return Err(err.to_string());
            }
        }
    }
}