use std::sync::Arc;
use crate::async_trait;
use serde::{Serialize, Deserialize};
use serde_json::{Value};
use crate::db::DB;
use crate::serenity::client::Context;
use crate::socket::types::SocketRequest;

pub struct GuildModulesRequest;

#[derive(Serialize, Deserialize)]
pub struct GuildModulesRequestData {
    pub guild_id: String,
}

#[derive(Serialize, Deserialize)]
pub struct GuildModulesResponse {
    pub modules: Vec<GuildModule>,
}

#[derive(Serialize, Deserialize)]
pub struct GuildModule {
    pub name: String,
}

#[async_trait]
impl SocketRequest for GuildModulesRequest {
    fn request_type(&self) -> &'static str {
        "guild_modules"
    }

    async fn execute(&self, data: Value, _user_id: String, db: Arc<DB>, _ctx: Arc<Context>) -> Result<String, String> {
        let data = serde_json::from_value::<GuildModulesRequestData>(data);

        match data {
            Ok(data) => {
                let modules: Vec<GuildModule> = db.get_enabled_modules(&data.guild_id).await
                    .into_iter().map(|x| {
                        GuildModule {
                            name: x.codename().to_string()
                        }
                    }).collect();

                let response = GuildModulesResponse {
                    modules
                };

                Ok(serde_json::to_string(&response).unwrap())
            }

            Err(err) => {
                return Err(err.to_string());
            }
        }
    }
}
