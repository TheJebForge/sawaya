use crate::db::{DB, process_result};

impl DB {
    pub async fn get_auth_key(&self, auth_key: &str) -> Option<String> {
        let rows = self.0.query(
            "select auth_key, user_id from bot.panel_auth
             where auth_key = $1",
            &[&auth_key]
        ).await;

        match rows {
            Ok(rows) => {
                if let Some(row) = rows.get(0) {
                    Some(row.get("user_id"))
                } else {
                    None
                }
            }
            Err(error) => {
                log::error!("Failed to get managers: {:?}", error);
                None
            }
        }
    }

    pub async fn add_auth_key(&self, auth_key: &str, user_id: &str) {
        if let None = self.get_auth_key(auth_key).await {
            process_result(self.0.execute("insert into bot.panel_auth (auth_key, user_id) values \
            ($1, $2);", &[&auth_key, &user_id]).await);
        }
    }

    pub async fn remove_auth_key(&self, auth_key: &str, user_id: &str) {
        if let Some(_) = self.get_auth_key(auth_key).await {
            process_result(self.0.execute("delete from bot.panel_auth \
            where auth_key = $1 and user_id = $2;", &[&auth_key, &user_id]).await);
        }
    }
}