use std::collections::HashMap;
use std::sync::Arc;
use serde_json::Value;
use tokio_tungstenite::tungstenite::Message;
use crate::db::DB;
use crate::serenity::client::Context;
use crate::socket::types::{BoxedSocketRequest, ErrorResponse, WebSocketPacket};

static mut SOCKET_REQUEST_TYPES: Vec<BoxedSocketRequest> = vec![];

pub fn get_request_type_map() -> HashMap<String, &'static BoxedSocketRequest> {
    unsafe {
        SOCKET_REQUEST_TYPES.iter().map(|x| (x.request_type().to_string(), x.clone())).collect()
    }
}

pub fn register_socket_request(request: BoxedSocketRequest) {
    unsafe {
        SOCKET_REQUEST_TYPES.push(request);
    }
}

pub async fn process_packet(user_id: String, request: &Message, db: Arc<DB>, ctx: Arc<Context>) -> Message {
    if let Ok(msg) = request.to_text() {
        match serde_json::from_str::<WebSocketPacket>(msg) {
            Ok(packet) => {
                let type_map = get_request_type_map();

                if let Some(request_type) = type_map.get(&packet.ty) {
                    process_for_type(request_type.clone(), user_id.to_string(), db.clone(), ctx.clone(), packet.data, packet.requester).await
                } else {
                    Message::Text(serde_json::to_string(&ErrorResponse {
                        error: format!("Unknown request type")
                    }).unwrap())
                }
            }

            Err(err) => {
                Message::Text(serde_json::to_string(&ErrorResponse {
                    error: format!("Failed to parse: {:?}", err)
                }).unwrap())
            }
        }
    } else {
        Message::Text(serde_json::to_string(&ErrorResponse {
            error: format!("Expected string")
        }).unwrap())
    }
}

async fn process_for_type(request: &BoxedSocketRequest, user_id: String, db: Arc<DB>, ctx: Arc<Context>, data: Option<HashMap<String, Value>>, requester: Option<String>) -> Message {
    let data = {
        match data {
            Some(map) => Value::Object(map.into_iter().collect()),
            None => Value::Object(HashMap::new().into_iter().collect())
        }
    };

    match request.execute(data, user_id, db.clone(), ctx.clone()).await {
        Ok(response) => {
            let packet = WebSocketPacket {
                requester,
                ty: request.request_type().to_string(),
                data: Some(serde_json::from_str(&response).unwrap())
            };

            Message::Text(serde_json::to_string(&packet).unwrap())
        }

        Err(err) => {
            Message::Text(serde_json::to_string(&ErrorResponse {
                error: format!("Request failed: {:?}", err)
            }).unwrap())
        }
    }
}