use serde::{Serialize, Deserialize};
use serde_json::Value;
use crate::events::BotEvent;

#[derive(Serialize, Deserialize, Debug)]
pub struct UserJoinedVC {
    pub guild: Option<String>,
    pub guild_id: Option<u64>,
    pub channel: Option<String>,
    pub channel_id: Option<u64>,
    pub user: String,
    pub display_name: String,
    pub user_id: u64,
    pub time: u64,
}

impl BotEvent for UserJoinedVC {
    fn event_type(&self) -> String {
        "vc_joined".to_string()
    }

    fn data(&self) -> Value {
        serde_json::to_value(self).unwrap()
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct UserLeftVC {
    pub guild: Option<String>,
    pub guild_id: Option<u64>,
    pub channel: Option<String>,
    pub channel_id: Option<u64>,
    pub user: String,
    pub display_name: String,
    pub user_id: u64,
    pub time: u64,
}

impl BotEvent for UserLeftVC {
    fn event_type(&self) -> String {
        "vc_left".to_string()
    }

    fn data(&self) -> Value {
        serde_json::to_value(self).unwrap()
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct UserMovedVC {
    pub guild: Option<String>,
    pub guild_id: Option<u64>,
    pub from_channel: Option<String>,
    pub from_channel_id: Option<u64>,
    pub to_channel: Option<String>,
    pub to_channel_id: Option<u64>,
    pub user: String,
    pub display_name: String,
    pub user_id: u64,
    pub time: u64,
}

impl BotEvent for UserMovedVC {
    fn event_type(&self) -> String {
        "vc_moved".to_string()
    }

    fn data(&self) -> Value {
        serde_json::to_value(self).unwrap()
    }
}