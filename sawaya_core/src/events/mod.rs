pub mod manager;
pub mod message;
pub mod voice_channel;

use std::any::Any;
use std::collections::HashMap;
use std::fmt::Debug;
use std::sync::{Arc};
use std::time::{SystemTime, UNIX_EPOCH};
use futures_util::lock::Mutex;
use serde_json::Value;
use crate::async_trait;



static mut EVENT_LISTENERS: Vec<(u64, BoxedEventListener)> = vec![];

pub fn call_event(event: BoxedBotEvent) {
    tokio::spawn(async {
        let event = Arc::new(event);

        for (_, listener) in unsafe { &EVENT_LISTENERS } {
            listener.event(event.clone()).await
        }

        log::info!(target : "{event_log}", "{}", serde_json::to_string(&event.to_map()).unwrap())
    });
}

pub fn bot_event_listener(listener: BoxedEventListener) -> BotEventListenerHandle {
    let time = SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_secs();

    unsafe { EVENT_LISTENERS.push((time, listener)); };
    
    BotEventListenerHandle {
        id: time
    }
}

pub fn bot_event_listener_permanent(listener: BoxedEventListener) {
    let time = SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_secs();

    unsafe { EVENT_LISTENERS.push((time, listener)); }
}

pub struct BotEventListenerHandle {
    pub id: u64
}

impl Drop for BotEventListenerHandle {
    fn drop(&mut self) {
        unsafe { EVENT_LISTENERS.retain(|(id, _)| *id == self.id) }
    }
}


pub struct BotEventPoolListener {
    pub pool: Option<Arc<Mutex<BotEventPool>>>
}

#[async_trait]
impl BotEventListener for BotEventPoolListener {
    async fn event(&self, event: Arc<BoxedBotEvent>) {
        if let Some(pool) = &self.pool {
            pool.lock().await.events.push(event.clone());
        }
    }
}

pub struct BotEventPool {
    pub events: Vec<Arc<BoxedBotEvent>>
}

impl BotEventPool {
    pub fn has_next(&self) -> bool {
        self.events.len() > 0
    }

    pub fn next(&mut self) -> Option<Arc<BoxedBotEvent>> {
        self.events.pop()
    }
}

pub async fn bot_event_pool() -> (BotEventListenerHandle, Arc<Mutex<BotEventPool>>) {
    let pool = Arc::new(Mutex::new(BotEventPool {
        events: vec![]
    }));

    let listener = BotEventPoolListener {
        pool: Some(pool.clone())
    };

    let handle = bot_event_listener(Box::new(listener));

    (handle, pool.clone())
}



pub type BoxedEventListener = Box<dyn BotEventListener + Send + Sync>;
pub type BoxedBotEvent = Box<dyn BotEvent + Send + Sync>;

#[async_trait]
pub trait BotEventListener {
    async fn event(&self, event: Arc<BoxedBotEvent>);
}

pub trait BotEvent: Any + Debug {
    fn event_type(&self) -> String;
    fn data(&self) -> Value;

    fn to_map(&self) -> HashMap<String, Value> {
        let mut map = HashMap::new();

        map.insert("event_type".to_string(), Value::String(self.event_type()));
        map.insert("data".to_string(), self.data());

        map
    }
}