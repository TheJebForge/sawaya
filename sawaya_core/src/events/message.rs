use serde::{Serialize, Deserialize};
use serde_json::Value;
use crate::events::BotEvent;

#[derive(Serialize, Deserialize, Debug)]
pub struct MessageEvent {
    pub author: String,
    pub author_id: u64,
    pub display_name: String,
    pub guild: Option<String>,
    pub guild_id: Option<u64>,
    pub channel: String,
    pub channel_id: u64,
    pub content: String,
    pub time: u64,
}

impl BotEvent for MessageEvent {
    fn event_type(&self) -> String {
        "message".to_string()
    }

    fn data(&self) -> Value {
        serde_json::to_value(self).unwrap()
    }
}