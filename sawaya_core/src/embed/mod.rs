use serenity::utils::Color;
use serenity::builder::Timestamp;
use chrono::Utc;

pub struct EmbedData {
    pub title: String,
    pub desc: Option<String>,
    pub color: u32,
    pub fields: Vec<(String, String, bool)>,
    pub timestamp: Option<Timestamp>,
}

impl EmbedData {
    pub fn new() -> EmbedData {
        EmbedData {
            title: "no title".to_string(),
            desc: None,
            color: Color::PURPLE.0,
            fields: vec![],
            timestamp: None
        }
    }

    pub fn title(mut self, str: String) -> EmbedData {
        self.title = str; self
    }

    pub fn desc(mut self, str: String) -> EmbedData {
        self.desc = Some(str); self
    }

    pub fn color(mut self, color: Color) -> EmbedData {
        self.color = color.0; self
    }

    pub fn add_field(&mut self, name: &str, value: &str, inline: bool) {
        self.fields.push((name.to_string(), value.to_string(), inline));
    }

    pub fn timestamp_to_current(mut self) -> EmbedData {
        self.timestamp = Some(Timestamp::from(&Utc::now())); self
    }

    pub fn failed(mut self) -> EmbedData {
        self.color = Color::RED.0; self
    }

    pub fn succeeded(mut self) -> EmbedData {
        self.color = Color::PURPLE.0; self
    }
}