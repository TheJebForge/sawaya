use crate::serenity::model::user::User;

#[derive(Debug, Clone)]
pub enum Argument {
    None,
    Integer(i32),
    Float(f32),
    String(String),
    Text(String),
    Mention(User),
}

impl Argument {
    pub fn argument_type(&self) -> String {
        match self {
            Argument::None => format!("None"),
            Argument::Integer(_) => format!("Integer"),
            Argument::Float(_) => format!("Float"),
            Argument::String(_) => format!("String"),
            Argument::Text(_) => format!("Text"),
            Argument::Mention(_) => format!("Mention"),
        }
    }

    pub fn to_string(&self) -> String {
        if let Argument::String(str) = self {
            str.to_string()
        } else {
            "".to_string()
        }
    }

    pub fn to_int(&self) -> i32 {
        if let Argument::Integer(int) = self {
            *int
        } else {
            0
        }
    }

    pub fn to_float(&self) -> f32 {
        if let Argument::Float(float) = self {
            *float
        } else {
            0.0
        }
    }

    pub fn to_text(&self) -> String {
        if let Argument::Text(str) = self {
            str.to_string()
        } else {
            "".to_string()
        }
    }

    pub fn to_user(&self) -> User {
        if let Argument::Mention(user) = self {
            user.clone()
        } else {
            panic!("You trying to get user from wrong argument, c'mon. Either something broke, or you dumb");
        }
    }
}