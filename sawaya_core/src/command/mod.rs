use crate::serenity::async_trait;
use crate::command::data::CommandData;
use crate::command::info::CommandInfo;
use crate::embed::EmbedData;
use crate::trn;

pub mod parameters;
pub mod arguments;
pub mod data;
pub mod info;
pub mod chain;

pub type BoxedCommand = Box<dyn BotCommand + Sync + Send>;

#[async_trait]
pub trait BotCommand {
    fn info(&self) -> CommandInfo;
    async fn run(&self, data: CommandData) -> Vec<EmbedData>;
}

pub fn no_run(data: CommandData) -> Vec<EmbedData> {
    vec![
        EmbedData::new()
            .failed()
            .title(trn!(data.lang, "error.no_run.title"))
            .desc(trn!(data.lang, "error.no_run.desc"))
    ]
}
