use std::fmt::{Display, Formatter};
use serde::{Serialize, Deserialize};

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
pub enum Parameter {
    Required(ParameterType),
    Optional(ParameterType)
}

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
pub enum ParameterType {
    Integer,
    Float,
    String,
    Text,
    Mention
}

impl Parameter {
    pub fn get_type(&self) -> &ParameterType {
        match self {
            Parameter::Required(ty) => ty,
            Parameter::Optional(ty) => ty,
        }
    }
}

impl Display for Parameter {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Parameter::Required(ty) => ty.fmt(f),
            Parameter::Optional(ty) => ty.fmt(f),
        }
    }
}

impl Display for ParameterType {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            ParameterType::Integer => write!(f, "Integer"),
            ParameterType::Float => write!(f, "Float"),
            ParameterType::String => write!(f, "String"),
            ParameterType::Text => write!(f, "Text"),
            ParameterType::Mention => write!(f, "Mention")
        }
    }
}