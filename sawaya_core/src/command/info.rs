use std::fmt::Display;
use crate::command::parameters::Parameter;
use crate::command::BoxedCommand;
use crate::permissions::BotPermission;

pub struct CommandInfo {
    pub id: String,
    pub name: String,
    pub parameters: Vec<Parameter>,
    pub children: Option<Vec<BoxedCommand>>,
    pub permission: BotPermission,
}

impl CommandInfo {
    pub fn new<I, N>(id: I, name: N) -> CommandInfo
        where
            I: Display,
            N: Display,
    {
        CommandInfo {
            id: id.to_string(),
            name: name.to_string(),
            parameters: vec![],
            children: None,
            permission: BotPermission::ANYONE
        }
    }

    pub fn parameters(mut self, parameters: &[Parameter]) -> CommandInfo {
        self.parameters = parameters.to_vec(); self
    }

    pub fn children(mut self, children: Vec<BoxedCommand>) -> CommandInfo {
        self.children = Some(children); self
    }

    pub fn permission(mut self, permission: BotPermission) -> CommandInfo {
        self.permission = permission; self
    }
}