use std::collections::HashMap;
use std::sync::Arc;
use crate::async_trait;
use crate::trn;
use crate::command::data::CommandData;
use crate::command::parameters::Parameter;
use crate::embed::EmbedData;
use crate::processor::args::process_arguments;

pub type BoxedChainCommand<T> = Box<dyn ChainCommand<T> + Sync + Send>;

#[async_trait]
pub trait ChainCommand<T> {
    fn info(&self) -> ChainCommandInfo;
    async fn run(&self, shared: &mut T, data: CommandData) -> Result<(), EmbedData>;
}

pub async fn process_chain<T>(mut shared: T, commands: Vec<BoxedChainCommand<T>>, data: CommandData) -> Result<T, EmbedData> {
    let map: HashMap<String, &BoxedChainCommand<T>> = commands.iter().map(|x| (x.info().name.to_string(), x)).collect();

    let args = data.raw_args();
    let mut args_iter = args.iter().peekable();

    while let Some(command) = args_iter.next() {
        let command = command.trim();
        if let Some(command_struct) = map.get(command) {
            match process_arguments(data.ctx.clone(), &mut args_iter, command_struct.info().parameters, data.lang.clone()).await {
                Ok(arguments) => {
                    let mut command_data = data.clone();

                    command_data.args = Arc::new(arguments);

                    if let Err(error) = command_struct.run(&mut shared, command_data).await {
                        return Err(error)
                    }
                }

                Err(error) => {
                    return Err(error)
                }
            }
        } else {
            return Err(
                EmbedData::new()
                    .title(trn!(data.lang, "error.unknown_command.title"))
                    .desc(trn!(data.lang, "error.unknown_command.desc", command))
                    .failed()
            )
        }
    }

    Ok(shared)
}

pub struct ChainCommandInfo {
    pub id: &'static str,
    pub name: &'static str,
    pub parameters: &'static [Parameter],
}

impl ChainCommandInfo {
    pub fn new(id: &'static str, name: &'static str) -> ChainCommandInfo {
        ChainCommandInfo {
            id,
            name,
            parameters: &[],
        }
    }

    pub fn parameters(mut self, parameters: &'static [Parameter]) -> ChainCommandInfo {
        self.parameters = parameters; self
    }
}