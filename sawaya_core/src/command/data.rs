use crate::command::arguments::Argument;
use crate::serenity::prelude::Context;
use crate::serenity::model::channel::Message;
use std::sync::Arc;
use crate::db::DB;
use crate::lang::Lang;
use crate::config::{Config, SensitiveConfig};

#[allow(dead_code)]
#[derive(Clone)]
pub struct CommandData {
    pub args: Arc<Vec<Argument>>,
    pub raw_args: Arc<Vec<String>>,
    pub ctx: Arc<Context>,
    pub message: Arc<Message>,
    pub db: Arc<DB>,
    pub lang: Arc<Lang>,
    pub config: Arc<Config>,
    pub sensitive: Arc<SensitiveConfig>,
    pub guild_id: String,
}

impl CommandData {
    pub fn raw_args(&self) -> Vec<String> {
        self.raw_args.iter().map(|x| x.to_string()).collect()
    }
}