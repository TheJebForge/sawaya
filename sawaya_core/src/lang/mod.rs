use std::collections::HashMap;
use crate::config::Config;
use toml::Value;
use std::sync::Arc;
use rand::Rng;
use toml::map::Map;

#[allow(dead_code)]
static mut LANGUAGES: Vec<(String, Arc<Lang>)> = vec![];

#[allow(dead_code)]
pub fn load_lang_files() {
    let config = Config::get();
    let mut langs = vec![];

    if let Ok(files) = std::fs::read_dir(config.lang_location) {
        for file in files {
            let file = file.expect("Couldn't load language file");

            if file.path().is_file() {
                // Got language file
                let contents = std::fs::read_to_string(file.path()).expect("Couldn't read language file");

                let file_path = file.path();
                let file_stem = file_path.file_stem().unwrap();
                let name = file_stem.to_str().expect("Couldn't finish parsing language file");
                langs.push((name.to_string(), Arc::new(Lang::parse(&contents))));
            }
        }
    }

    log::info!("Loaded languages: {:?}", langs.iter().map(|x| x.0.to_string()).collect::<Vec<String>>());

    unsafe {
        LANGUAGES = langs;
    }
}

#[allow(dead_code)]
pub fn get_langs<'a>() -> HashMap<String, Arc<Lang>> {
    unsafe {
        LANGUAGES.iter().map(|x| {
            let (s, l) = x;
            (s.to_string(), l.clone())
        }).collect::<HashMap<String, Arc<Lang>>>()
    }
}

pub fn get_lang<'a>(name: &str) -> Arc<Lang> {
    if let Some(lang) = get_langs().remove(name) {
        lang
    } else {
        Arc::new(Lang(Value::Table(Map::new())))
    }
}


#[allow(dead_code)]
pub struct Lang(Value);

#[allow(dead_code)]
impl Lang {
    fn get_value_on_key(&self, key: &str) -> Option<Value> {
        let keys = key.split(".");

        let mut table = self.0.as_table().unwrap();

        for key in keys {
            let value = table.get(key);

            if let Some(value) = value {
                if let Value::Table(t) = value {
                    table = t;
                } else {
                    return Some(value.clone())
                }
            }
        }

        return None
    }

    pub fn parse(str: &str) -> Lang {
        Lang(str.parse::<Value>().expect("Couldn't parse language file"))
    }

    pub fn format(&self, key: &str, values: Vec<&str>) -> String {
        let mut result: String = if let Some(value) = self.get_value_on_key(key) {
            if let Value::String(str) = value {
                str
            } else {
                format!("{} - key's format is invalid", key)
            }
        } else {
            key.to_string()
        };

        for (i, value) in values.iter().enumerate() {
            result = result.replace(format!("[{}]", i + 1).as_str(), value);
        }

        result
    }

    pub fn random_module_motto(&self) -> String {
        if let Some(array) = self.get_value_on_key("mod_motto") {
            if let Value::Array(array) = array {
                let mut rng = rand::thread_rng();

                if let Some(item) = array.get(rng.gen_range(0..array.len())) {
                    if let Value::String(str) = item {
                        str.to_string()
                    } else {
                        "key's format is invalid".to_string()
                    }
                } else { "".to_string() }
            } else { "".to_string() }
        } else { "".to_string() }
    }
}