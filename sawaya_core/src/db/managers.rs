use crate::db::{DB, process_result};

impl DB {
    pub async fn get_managers(&self, guild_id: &str) -> Vec<String> {
        let mut managers = vec![];

        let rows = self.0.query(
            "select guild_id, user_id from bot.guild_managers
             where guild_id = $1",
            &[&guild_id]
        ).await;

        match rows {
            Ok(rows) => {
                for row in rows {
                    managers.push(row.get("user_id"));
                }
            }
            Err(error) => log::error!("Failed to get managers: {:?}", error),
        }

        managers
    }

    pub async fn get_manager_guilds(&self, user_id: &str) -> Vec<String> {
        let mut guilds = vec![];

        let rows = self.0.query(
            "select guild_id, user_id from bot.guild_managers
             where user_id = $1",
            &[&user_id]
        ).await;

        match rows {
            Ok(rows) => {
                for row in rows {
                    guilds.push(row.get("guild_id"));
                }
            }
            Err(error) => log::error!("Failed to get guilds: {:?}", error),
        }

        guilds
    }

    pub async fn add_manager(&self, guild_id: &str, user_id: &str) {
        if !self.get_managers(guild_id).await.contains(&user_id.to_string()) {
            process_result(self.0.execute("insert into bot.guild_managers (guild_id, user_id) values \
            ($1, $2);", &[&guild_id, &user_id]).await);
        }
    }

    pub async fn remove_manager(&self, guild_id: &str, user_id: &str) {
        if self.get_managers(guild_id).await.contains(&user_id.to_string()) {
            process_result(self.0.execute("delete from bot.guild_managers \
            where guild_id = $1 and user_id = $2;", &[&guild_id, &user_id]).await);
        }
    }
}