use crate::db::{DB, process_result};
use crate::{get_modules, BoxedModule};

impl DB {
    pub async fn get_enabled_module_names(&self, guild_id: &str) -> Vec<String> {
        let mut enabled_modules = vec![];

        let rows = self.0.query(
            "select guild_id, codename from bot.guild_module
             where guild_id = $1",
            &[&guild_id]
        ).await;

        match rows {
            Ok(rows) => {
                for row in rows {
                    enabled_modules.push(row.get("codename"));
                }
            }
            Err(error) => log::error!("Failed to get enabled modules: {:?}", error),
        }

        enabled_modules
    }

    pub async fn get_enabled_modules(&self, guild_id: &str) -> Vec<&'static BoxedModule> {
        let mut modules = get_modules();
        let enabled_modules = self.get_enabled_module_names(guild_id).await;
        let mut result = vec![];

        for enabled_module in enabled_modules {
            let module = modules.remove(&enabled_module);

            if let Some(module) = module {
                result.push(module);
            }
        }

        result
    }

    pub async fn add_module(&self, guild_id: &str, codename: &str) {
        if !self.get_enabled_module_names(guild_id).await.contains(&codename.to_string()) {
            process_result(self.0.execute("insert into bot.guild_module (guild_id, codename) values \
            ($1, $2);", &[&guild_id, &codename]).await);
        }
    }

    pub async fn remove_module(&self, guild_id: &str, codename: &str) {
        if self.get_enabled_module_names(guild_id).await.contains(&codename.to_string()) {
            process_result(self.0.execute("delete from bot.guild_module \
            where guild_id = $1 and codename = $2;", &[&guild_id, &codename]).await);
        }
    }
}