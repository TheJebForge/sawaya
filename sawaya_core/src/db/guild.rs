use tokio_postgres::Row;
use crate::db::{DB, process_result};

impl DB {
    pub async fn set_guild(&self, guild_id: &str, language: String) {
        if self.get_guild(guild_id).await.is_some() {
            process_result(self.0.execute("UPDATE bot.guild
                SET language = $1
                WHERE guild_id = $2;", &[&language, &guild_id]).await);
        } else {
            process_result(self.0.execute("INSERT INTO bot.guild(
                guild_id, language)
                VALUES ($2, $1);", &[&language, &guild_id]).await);
        }
    }

    pub async fn get_guild(&self, guild_id: &str) -> Option<GuildData> {
        let rows = self.0.query(
            "select * from bot.guild
            where guild_id = $1",
            &[&guild_id]
        ).await;

        match rows {
            Ok(rows) => {
                if rows.len() > 0 {
                    Some(GuildData::from_row(&rows[0]))
                } else {
                    None
                }
            }
            Err(error) => {
                log::error!("Failed to get enabled modules: {:?}", error);
                None
            },
        }
    }
}

pub struct GuildData {
    pub guild_id: String,
    pub language: String
}

impl GuildData {
    pub fn from_row(row: &Row) -> GuildData {
        GuildData {
            guild_id: row.get("guild_id"),
            language: row.get("language")
        }
    }
}