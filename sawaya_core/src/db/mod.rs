pub mod guild;
mod modules;
mod managers;

use crate::config::SensitiveConfig;
use tokio_postgres::{NoTls, Client, Error};
use std::sync::Arc;

pub struct DB(pub Client);

impl DB {
    pub async fn connect() -> Arc<DB> {
        let con_str = SensitiveConfig::get().database_connection_string;

        let (client, connection) =
            tokio_postgres::connect(&con_str, NoTls).await.expect("Failed to connect to database");

        log::info!("Connected to database");

        tokio::spawn(async move {
            if let Err(e) = connection.await {
                log::error!("Connection error: {}", e)
            }
        });

        Arc::new(DB(client))
    }
}

pub fn process_result(result: Result<u64, Error>) {
    if let Err(error) = result {
        log::error!("Failed to execute update: {:?}", error);
    }
}