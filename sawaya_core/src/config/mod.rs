mod sensitive;
pub use sensitive::SensitiveConfig;

use serde::Deserialize;
use std::fs;

#[allow(dead_code)]
#[derive(Deserialize)]
pub struct Config {
    pub prefix: String,
    pub lang_location: String,
    pub frontend_path: String,
    pub sensitive_path: String,
    pub websocket_bind: String,
}

#[allow(dead_code)]
impl Config {
    pub fn get() -> Config {
        toml::from_str(&fs::read_to_string("config.toml")
            .expect("Failed to load config"))
            .expect("Missing fields in config")
    }
}