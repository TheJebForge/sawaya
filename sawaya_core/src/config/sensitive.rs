use serde::Deserialize;
use std::fs;
use crate::config::Config;

#[allow(dead_code)]
#[derive(Deserialize)]
pub struct SensitiveConfig {
    pub token: String,
    pub database_connection_string: String,
    pub creator_id: String,

    pub vp_endpoint: String,
    pub vp_key: String,
}

#[allow(dead_code)]
impl SensitiveConfig {
    pub fn get() -> SensitiveConfig {
        toml::from_str(&fs::read_to_string(Config::get().sensitive_path)
            .expect("Failed to load sensitive config"))
            .expect("Missing fields in sensitive config")
    }
}