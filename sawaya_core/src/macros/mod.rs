#[macro_export]
macro_rules! trn {
    ($e:expr, $key:expr) => {
        {
            $e.format($key, vec![])
        }
    };

    ($e:expr, $key:expr, $($value:expr),*) => {
        {
            $e.format($key, vec![$(format!("{}", $value).as_str()),*])
        }
    }
}

#[macro_export]
macro_rules! block {
    ($e:expr) => {
        {
            tokio::task::block_in_place(|| {serenity::futures::executor::block_on($e)})
        }
    };
}