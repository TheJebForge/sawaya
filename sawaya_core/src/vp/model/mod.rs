use serde::{Serialize, Deserialize};
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct VoiceProfile {
    pub id: String,
    pub user_id: String,
    pub guild_id: String,
    pub pray: Pray,
    pub timespent: HashMap<String, i64>,
    pub level: i64,
    pub experience: i64,
    pub voicepoints: i64,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Pray {
    pub date: u64,
    pub streak: i64,
    pub total: i64,
}