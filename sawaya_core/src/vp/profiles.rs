use crate::vp::VoiceProfileClient;
use crate::vp::model::VoiceProfile;

impl VoiceProfileClient {
    pub async fn get_top(&self, guild_id: Option<&str>, by: &str) -> Result<Vec<VoiceProfile>, String> {
        let endpoint = if let Some(guild_id) = guild_id {
            format!("profile/{}/top?by={}", guild_id, by)
        } else {
            format!("profile/top?by={}", by)
        };

        match self.client.get(self.endpoint(&endpoint)).send().await {
            Ok(resp) => {
                if resp.status() == 200 {
                    if let Ok(body) = resp.text().await {
                        match serde_json::from_str(&body) {
                            Ok(v) => Ok(v),
                            Err(_) => Err(format!("Couldn't parse response: {}", body))
                        }
                    } else {
                        Err("No response".to_string())
                    }
                } else {
                    Err(format!("Unknown response: {:?}", resp))
                }
            }
            Err(_) => Err("Couldn't send request".to_string()),
        }
    }
}