pub mod model;
mod profiles;

use reqwest::{Client, ClientBuilder};
use crate::config::SensitiveConfig;
use std::sync::Arc;
use reqwest::header::HeaderMap;

pub struct VoiceProfileClient {
    base_url: String,
    client: Client,
}

impl VoiceProfileClient {
    pub fn init(sensitive: Arc<SensitiveConfig>) -> Arc<VoiceProfileClient> {
        let mut headers = HeaderMap::new();
        headers.insert("Authorization", sensitive.vp_key.parse().unwrap());

        let client = ClientBuilder::new()
            .default_headers(headers)
            .build().unwrap();

        Arc::new(VoiceProfileClient {
            base_url: sensitive.vp_endpoint.to_string(),
            client
        })
    }

    fn endpoint(&self, endpoint: &str) -> String {
        format!("{}{}", self.base_url, endpoint)
    }
}