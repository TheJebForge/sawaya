use serenity::model::channel::Message;
use std::sync::Arc;
use crate::permissions::BotPermission;
use crate::config::SensitiveConfig;
use crate::db::DB;
use serenity::prelude::Context;
use crate::processor::guild_id_from_msg;
use crate::command::data::CommandData;

pub async fn get_permission_access_level_from_data(data: &CommandData) -> BotPermission {
    get_permission_access_level(data.db.clone(), data.ctx.clone(), data.message.clone(), data.sensitive.clone()).await
}

pub async fn get_permission_access_level(db: Arc<DB>, ctx: Arc<Context>, message: Arc<Message>, sensitive: Arc<SensitiveConfig>) -> BotPermission {
    let guild_id = guild_id_from_msg(ctx.clone(), message.clone()).await;

    let user_id = message.author.id.to_string();

    if user_id == sensitive.creator_id {
        BotPermission::CREATOR
    } else {
        let managers = db.get_managers(&guild_id).await;

        if managers.contains(&user_id) {
            BotPermission::MANAGERS
        } else {
            BotPermission::ANYONE
        }
    }
}