use std::fmt::{Display, Formatter};
use crate::embed::EmbedData;
use crate::lang::Lang;
use std::sync::Arc;
use crate::trn;
use serde::{Serialize, Deserialize};

pub mod processor;

#[derive(Serialize, Deserialize, Clone, Copy, Debug)]
pub enum BotPermission {
    ANYONE,
    MANAGERS,
    CREATOR
}

impl BotPermission {
    /// Compares permission levels and gives response if permission is denied
    pub fn check(&self, lang: Arc<Lang>, expected: &BotPermission) -> Result<(), EmbedData> {
        if self.compare(expected) {
            Ok(())
        } else {
            let translated_level = match expected {
                BotPermission::ANYONE => trn!(lang, "error.permission_denied.anyone"),
                BotPermission::MANAGERS => trn!(lang, "error.permission_denied.manager"),
                BotPermission::CREATOR => trn!(lang, "error.permission_denied.creator"),
            };

            Err(EmbedData::new()
                .title(trn!(lang, "error.permission_denied.title", translated_level))
                .desc(trn!(lang, "error.permission_denied.desc", translated_level))
                .failed())
        }
    }

    /// Compares if this enum's level is equal or greater than expected
    pub fn compare(&self, expected: &BotPermission) -> bool {
        match expected {
            BotPermission::ANYONE => true,
            BotPermission::MANAGERS => {
                match self {
                    BotPermission::ANYONE => false,
                    _ => true,
                }
            }
            BotPermission::CREATOR => {
                match self {
                    BotPermission::CREATOR => true,
                    _ => false,
                }
            }
        }
    }
}

impl Display for BotPermission {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", match self {
            BotPermission::ANYONE => format!("Anyone"),
            BotPermission::MANAGERS => format!("Managers"),
            BotPermission::CREATOR => format!("Creator"),
        })
    }
}