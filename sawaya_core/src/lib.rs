use std::collections::HashMap;

pub use serenity;
pub use serenity::async_trait;

use crate::command::BoxedCommand;
// Module loading
use crate::module::BotModule;

pub mod config;
pub mod command;
pub mod module;
pub mod processor;
pub mod db;
pub mod lang;
pub mod macros;
pub mod embed;
pub mod permissions;
pub mod vp;
pub mod lua;
pub mod socket;
pub mod events;

pub type BoxedModule = Box<dyn BotModule + Sync + Send>;

static mut LOADED_MODULES: Vec<BoxedModule> = vec![];

pub fn add_boxed_module(module: BoxedModule) {
    log::info!("Loaded module: {:?}", module.codename());

    unsafe {
        LOADED_MODULES.push(module);
    }
}

pub fn get_modules() -> HashMap<String, &'static BoxedModule> {
    unsafe {
        LOADED_MODULES.iter().map(|m| (m.codename().to_string(), m)).collect()
    }
}

pub fn get_module_commands() -> HashMap<String, Vec<BoxedCommand>> {
    let mut map = HashMap::new();

    let modules = get_modules();

    for module in modules.values() {
        let commands = module.commands();
        map.insert(module.codename().to_string(), commands);
    }

    map
}
