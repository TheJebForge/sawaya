use std::ops::Deref;
use std::sync::Arc;

use serenity::model::channel::{Channel, Message};
use serenity::prelude::*;

use crate::get_module_commands;
use crate::config::{Config, SensitiveConfig};
use crate::db::DB;
use crate::events::call_event;
use crate::events::message::MessageEvent;
use crate::lang::get_lang;
use crate::processor::args::process_tree;

pub mod args;
pub mod embed;

static DEFAULT_MODULES: &'static [&'static str] = &["config_module"];
const DEFAULT_LANGUAGE: &str = "english";

pub async fn process_commands(ctx: Arc<Context>, message: Arc<Message>, db: Arc<DB>, config: Arc<Config>, sensitive: Arc<SensitiveConfig>) {
    log_message(ctx.clone(), message.clone()).await;
    call_message_event(ctx.clone(), message.clone()).await;

    if !message.author.bot {
        if message.content.starts_with(&config.prefix) {
            let guild_id = guild_id_from_msg(ctx.clone(), message.clone()).await;

            let owner_id = {
                if let Some(guild) = message.guild_id {
                    if let Ok(guild) = guild.to_partial_guild(ctx.deref()).await {
                        guild.owner_id.0.to_string()
                    } else {
                        "".to_string()
                    }
                } else if let Ok(channel) = message.channel_id.to_channel(ctx.deref()).await {
                    if let Channel::Private(private) = channel {
                        private.recipient.id.0.to_string()
                    } else {
                        "".to_string()
                    }
                } else {
                    "".to_string()
                }
            };

            let lang = {
                let default = get_lang(DEFAULT_LANGUAGE);

                let guild_data = db.get_guild(&guild_id).await;

                if let Some(guild_data) = guild_data {
                    get_lang(&guild_data.language)
                } else {
                    log::info!("Registering guild with id: {}", guild_id);

                    db.set_guild(&guild_id, DEFAULT_LANGUAGE.to_string()).await;

                    for module in DEFAULT_MODULES {
                        db.add_module(&guild_id, module).await;
                    }

                    db.add_manager(&guild_id, &owner_id).await;
                    db.add_manager(&guild_id, &sensitive.creator_id).await;

                    default
                }
            };

            let arguments = message.content[config.prefix.len()..].replace('\n', " \n").split(" ").filter(|&x| !x.is_empty()).map(|x| x.to_string()).collect::<Vec<String>>();
            let mut argument_iterator = arguments.iter().peekable();

            let guild_modules = db.get_enabled_module_names(&guild_id).await;

            if let Some(command) = argument_iterator.next() {
                for (module_name, command_structs) in get_module_commands() {

                    if guild_modules.contains(&module_name) {
                        for command_struct in command_structs {
                            if command_struct.info().name.as_str() == command {
                                return process_tree(ctx.clone(), message.clone(), db.clone(), config, sensitive, &command_struct, command_struct.info().children, &mut argument_iterator, lang.clone(), guild_id).await;
                            }
                        }
                    }
                }

                log::info!("Unknown command");
            } else {
                if let Err(t) = message.react(ctx.deref(), '❓').await {
                    log::error!("Failed to react: {:?}", t);
                }
            }
        }
    }
}

async fn log_message(ctx: Arc<Context>, message: Arc<Message>) {
    let channel_type = message.channel_id.to_channel(ctx.deref()).await.unwrap();

    let self_user = ctx.cache.current_user().await;
    let me = if message.author.id == self_user.id {"<me> "} else {""};

    if let Channel::Guild(channel) = channel_type {
        // If guild channel
        if let Some(guild) = &message.guild_id {
            let partial_guild = guild.to_partial_guild(ctx).await.unwrap();

            if let Some(member) = &message.member {
                if let Some(nick) = &member.nick {
                    log::info!("{}{} #{} {} ({}#{:04}): {}", me, partial_guild.name, channel.name, nick, message.author.name, message.author.discriminator, message.content);
                } else {
                    log::info!("{}{} #{} {}#{:04}: {}", me, partial_guild.name, channel.name, message.author.name, message.author.discriminator, message.content);
                }
            }
        }

    } else if let Channel::Private(_) = channel_type{
        // If private channel
        log::info!("DM {}#{:04}: {}", message.author.name, message.author.discriminator, message.content);
    }
}

async fn call_message_event(ctx: Arc<Context>, message: Arc<Message>) {
    let author = format!("{}#{:04}", message.author.name, message.author.discriminator);

    let event = MessageEvent {
        author: author.to_string(),
        author_id: message.author.id.0,
        display_name: if let Some(member) = &message.member {
            member.nick.as_ref().unwrap_or(&author.to_string()).to_string()
        } else {
            author.to_string()
        },
        guild: if let Some(guild) = message.guild_id {
            Some(guild.name(ctx.deref()).await.unwrap_or(guild.0.to_string()))
        } else {
            None
        },
        guild_id: if let Some(guild) = message.guild_id {
            Some(guild.0)
        } else {
            None
        },
        channel: message.channel_id.name(ctx.deref()).await.unwrap_or(message.channel_id.0.to_string()),
        channel_id: message.channel_id.0,
        content: message.content.to_string(),
        time: message.timestamp.timestamp() as u64,
    };

    call_event(Box::new(event));
}

pub async fn guild_id_from_msg(ctx: Arc<Context>, message: Arc<Message>) -> String {
    if let Some(guild) = message.guild_id {
        guild.to_string()
    } else if let Ok(channel) = message.channel_id.to_channel(ctx.deref()).await {
        if let Channel::Private(private) = channel {
            private.id.to_string()
        } else {
            "".to_string()
        }
    } else {
        "".to_string()
    }
}