use std::sync::Arc;
use serenity::prelude::Context;
use serenity::model::channel::Message;
use crate::embed::EmbedData;
use serenity::builder::CreateEmbed;

pub fn process_embed(embed: &EmbedData) -> CreateEmbed {
    let mut result = CreateEmbed::default();

    result.title(&embed.title)
        .color(embed.color);

    if let Some(desc) = &embed.desc {
        result.description(desc);
    }

    if embed.fields.len() > 0 {
        for (name, value, inline) in &embed.fields {
            result.field(name, value, *inline);
        }
    }

    if let Some(timestamp) = &embed.timestamp {
        result.timestamp(timestamp.clone());
    }

    result
}

pub async fn send_embeds(ctx: Arc<Context>, message: Arc<Message>, embeds: Vec<EmbedData>) {
    for embed in embeds {
        let processed = process_embed(&embed);
        message.channel_id.send_message(&ctx.http, |m| {
            m.set_embed(processed)
        }).await.ok();
    }
}