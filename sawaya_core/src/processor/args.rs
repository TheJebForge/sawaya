use crate::command::{BoxedCommand};
use std::iter::{Peekable};
use serenity::prelude::Context;
use serenity::model::prelude::Message;
use crate::command::arguments::Argument;
use std::slice::Iter;
use async_recursion::async_recursion;
use crate::command::parameters::{Parameter, ParameterType};
use std::sync::Arc;
use crate::db::DB;
use crate::command::data::CommandData;
use crate::lang::Lang;
use crate::processor::embed::send_embeds;
use crate::embed::EmbedData;
use crate::trn;
use crate::config::{Config, SensitiveConfig};
use crate::permissions::processor::get_permission_access_level;
use serenity::model::id::UserId;
use std::ops::Deref;
use substring::Substring;

#[async_recursion]
pub async fn process_tree(ctx: Arc<Context>, message: Arc<Message>, db: Arc<DB>, config: Arc<Config>, sensitive: Arc<SensitiveConfig>, current_command: &BoxedCommand, child_commands: Option<Vec<BoxedCommand>>, args: &mut Peekable<Iter<'_, String>>, lang: Arc<Lang>, guild_id: String) {
    let possible_command = args.peek();

    if let Some(command) = possible_command {
        let diffused_command = command.as_str();

        if let Some(child_commands) = child_commands {
            for child_command in child_commands {
                if child_command.info().name == diffused_command {
                    // Found child command, going recursive
                    args.next();

                    process_tree(ctx, message, db.clone(), config, sensitive, &child_command, child_command.info().children, args, lang.clone(), guild_id).await;
                    return;
                }
            }

            // Haven't found any children, running original command
            run_command(ctx, message, db, config, sensitive, args, lang, current_command, guild_id).await;
        } else {
            // No children, just running the command
            run_command(ctx, message, db, config, sensitive, args, lang, current_command, guild_id).await;
        }
    } else {
        // No arguments, parsing anyway to get the error
        run_command(ctx, message, db, config, sensitive, args, lang, current_command, guild_id).await;
    }
}

pub async fn run_command(ctx: Arc<Context>, message: Arc<Message>, db: Arc<DB>, config: Arc<Config>, sensitive: Arc<SensitiveConfig>, args: &mut Peekable<Iter<'_, String>>, lang: Arc<Lang>, current_command: &BoxedCommand, guild_id: String) {
    let perm = get_permission_access_level(db.clone(), ctx.clone(), message.clone(), sensitive.clone()).await;

    match perm.check(lang.clone(), &current_command.info().permission) {
        Ok(_) => {
            match process_arguments(ctx.clone(), args, current_command.info().parameters.as_slice(), lang.clone()).await {
                Ok(arguments) => {
                    send_embeds(ctx.clone(), message.clone(), current_command.run(CommandData {
                        args: Arc::new(arguments),
                        raw_args: Arc::new(args.map(|x| x.to_string()).collect()),
                        ctx,
                        message,
                        db,
                        lang,
                        config,
                        sensitive,
                        guild_id
                    }).await).await;
                }

                Err(error) => {
                    send_embeds(ctx.clone(), message.clone(), vec![error]).await;
                }
            }
        }

        Err(embed) => {
            send_embeds(ctx.clone(), message.clone(), vec![embed]).await
        }
    }
}

pub async fn process_arguments(ctx: Arc<Context>, args: &mut Peekable<Iter<'_, String>>, expected_args: &[Parameter], lang: Arc<Lang>) -> Result<Vec<Argument>, EmbedData> {
    let mut parsed_args = vec![];

    for (i, expected_arg) in expected_args.iter().enumerate() {
        let arg = args.next();

        if let Some(arg) = arg {
            let translated_type = match expected_arg.get_type() {
                ParameterType::Integer => trn!(lang, "error.invalid_argument.integer"),
                ParameterType::Float => trn!(lang, "error.invalid_argument.float"),
                ParameterType::Mention => trn!(lang, "error.invalid_argument.mention"),
                _ => "".to_string(),
            };

            match expected_arg.get_type() {
                ParameterType::Integer => {
                    let int = arg.parse::<i32>();

                    if let Ok(int) = int {
                        parsed_args.push(Argument::Integer(int));
                    } else {
                        return Err(
                            EmbedData::new()
                                .failed()
                                .title(trn!(lang, "error.invalid_argument.title", translated_type))
                                .desc(trn!(lang, "error.invalid_argument.desc", translated_type, i + 1))
                        );
                    }
                }
                ParameterType::Float => {
                    let float = arg.parse::<f32>();

                    if let Ok(float) = float {
                        parsed_args.push(Argument::Float(float));
                    } else {
                        return Err(
                            EmbedData::new()
                                .failed()
                                .title(trn!(lang, "error.invalid_argument.title", translated_type))
                                .desc(trn!(lang, "error.invalid_argument.desc", translated_type, i + 1))
                        );
                    }
                }
                ParameterType::String => {
                    parsed_args.push(Argument::String(arg.to_string()));
                }
                ParameterType::Text => {
                    let mut text = arg.to_string() + " ";
                    text += &args.map(|x| x.to_string()).collect::<Vec<String>>().join(" ");
                    parsed_args.push(Argument::Text(text));
                }
                ParameterType::Mention => {
                    if arg.len() > 4 {
                        let id = arg.substring(2,arg.len() - 1);
                        if let Ok(id) = id.parse::<u64>() {
                            if let Ok(user) = UserId(id).to_user(ctx.deref()).await {
                                parsed_args.push(Argument::Mention(user));
                            } else {
                                return Err(
                                    EmbedData::new()
                                        .failed()
                                        .title(trn!(lang, "error.invalid_argument.title", translated_type))
                                        .desc(trn!(lang, "error.invalid_argument.desc", translated_type, i + 1))
                                );
                            }
                        } else if let Ok(id) = arg.substring(3,arg.len() - 1).parse::<u64>() {
                            if let Ok(user) = UserId(id).to_user(ctx.deref()).await {
                                parsed_args.push(Argument::Mention(user));
                            } else {
                                return Err(
                                    EmbedData::new()
                                        .failed()
                                        .title(trn!(lang, "error.invalid_argument.title", translated_type))
                                        .desc(trn!(lang, "error.invalid_argument.desc", translated_type, i + 1))
                                );
                            }
                        } else {
                            return Err(
                                EmbedData::new()
                                    .failed()
                                    .title(trn!(lang, "error.invalid_argument.title", translated_type))
                                    .desc(trn!(lang, "error.invalid_argument.desc", translated_type, i + 1))
                            );
                        }
                    } else {
                        return Err(
                            EmbedData::new()
                                .failed()
                                .title(trn!(lang, "error.invalid_argument.title", translated_type))
                                .desc(trn!(lang, "error.invalid_argument.desc", translated_type, i + 1))
                        );
                    }
                }
            }
        } else {
            if let Parameter::Required(ty) = expected_arg {
                let translated_type = match ty {
                    ParameterType::Integer => trn!(lang, "error.missing_argument.integer"),
                    ParameterType::Float => trn!(lang, "error.missing_argument.float"),
                    ParameterType::String => trn!(lang, "error.missing_argument.string"),
                    ParameterType::Text => trn!(lang, "error.missing_argument.text"),
                    ParameterType::Mention => trn!(lang, "error.missing_argument.mention")
                };

                return Err(
                    EmbedData::new()
                        .title(trn!(lang, "error.missing_argument.title", translated_type))
                        .desc(trn!(lang, "error.missing_argument.desc", translated_type))
                        .failed()
                );
            } else {
                parsed_args.push(Argument::None);
            }
        }
    }

    // TODO: Rick roll someone on a random day
    // TODO: Random rick roll on unsuccessful parsing

    Ok(parsed_args)
}