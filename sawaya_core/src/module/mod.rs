use crate::async_trait;
use crate::command::{BoxedCommand};
use std::sync::Arc;
use crate::lang::Lang;
use crate::embed::EmbedData;
use crate::trn;
use crate::permissions::BotPermission;
use serenity::client::Context;
use crate::db::DB;

pub fn module_about_embed(lang: Arc<Lang>, codename: &str) -> EmbedData {
    EmbedData::new()
        .title(format!("{} ({})", trn!(lang, &format!("modules.{}.name", codename)), codename))
        .desc(trn!(lang, &format!("modules.{}.desc", codename)))
}

#[async_trait]
pub trait BotModule {
    fn init(&self, _: Arc<Context>, _: Arc<DB>) {}

    fn about(&self, lang: Arc<Lang>) -> EmbedData {
        module_about_embed(lang, self.codename())
    }

    fn perm_level_to_add(&self) -> BotPermission {
        BotPermission::MANAGERS
    }

    fn can_be_disabled(&self) -> bool {
        true
    }

    fn codename(&self) -> &str;
    fn commands(&self) -> Vec<BoxedCommand>;
}