use sawaya_core::async_trait;
use sawaya_core::command::BotCommand;
use sawaya_core::command::info::CommandInfo;
use sawaya_core::command::data::CommandData;
use sawaya_core::embed::EmbedData;
use sawaya_core::permissions::BotPermission;
use sawaya_core::command::parameters::{Parameter, ParameterType};
use sawaya_core::lua::create_lua_state;
use std::sync::Arc;

pub struct DebugLuaCommand;

#[async_trait]
impl BotCommand for DebugLuaCommand {
    fn info(&self) -> CommandInfo {
        CommandInfo::new(
            "debug-lua",
            "lua"
        ).permission(
            BotPermission::CREATOR
        ).parameters(
            &[
                Parameter::Required(ParameterType::Text)
            ]
        )
    }

    async fn run(&self, data: CommandData) -> Vec<EmbedData> {
        let data = Arc::new(data);

        let lua = create_lua_state(data.clone());
        let result = lua.execute(data.args[0].to_text());

        match result {
            Ok(_) => {
                vec![
                    EmbedData::new()
                        .title("Execution successful".to_string())
                        .desc(format!("Output:\n```\n{}```", lua.get_log()))
                ]
            }
            Err(err) => {
                vec![
                    EmbedData::new()
                        .title("Execution failed".to_string())
                        .desc(err.to_string())
                        .failed()
                ]
            }
        }
    }
}