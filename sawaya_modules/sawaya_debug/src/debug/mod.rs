mod lua;
mod ping;

use sawaya_core::async_trait;
use sawaya_core::command::BotCommand;
use sawaya_core::command::info::CommandInfo;
use sawaya_core::command::data::CommandData;
use sawaya_core::embed::EmbedData;
use sawaya_core::permissions::BotPermission;
use crate::debug::lua::DebugLuaCommand;
use crate::debug::ping::DebugPingCommand;

pub struct DebugCommand;

#[async_trait]
impl BotCommand for DebugCommand {
    fn info(&self) -> CommandInfo {
        CommandInfo::new(
            "debug",
            "debug"
        ).permission(
            BotPermission::CREATOR
        ).children(
            vec![
                Box::new(DebugLuaCommand),
                Box::new(DebugPingCommand)
            ]
        )
    }

    async fn run(&self, _: CommandData) -> Vec<EmbedData> {
        vec![
            EmbedData::new()
                .title("Debug Module".to_string())
                .desc("Hi, this won't be translated to Russian because only I would fking use it".to_string())
        ]
    }
}