use serde::{Serialize, Deserialize};
use serde_json::Value;
use sawaya_core::async_trait;
use sawaya_core::command::BotCommand;
use sawaya_core::command::data::CommandData;
use sawaya_core::command::info::CommandInfo;
use sawaya_core::embed::EmbedData;
use sawaya_core::events::{BotEvent, call_event};
use sawaya_core::permissions::BotPermission;

pub struct DebugPingCommand;

#[async_trait]
impl BotCommand for DebugPingCommand {
    fn info(&self) -> CommandInfo {
        CommandInfo::new(
            "debug-ping",
            "ping"
        ).permission(
            BotPermission::CREATOR
        )
    }

    async fn run(&self, _data: CommandData) -> Vec<EmbedData> {
        call_event(Box::new(PingEvent));

        vec![
            EmbedData::new()
                .title("Attempted to inject event".to_string())
        ]
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct PingEvent;

impl BotEvent for PingEvent {
    fn event_type(&self) -> String {
        "debug_ping".to_string()
    }

    fn data(&self) -> Value {
        serde_json::to_value(self).unwrap()
    }
}