use std::sync::Arc;

use sawaya_core::command::BoxedCommand;
use sawaya_core::embed::EmbedData;
use sawaya_core::lang::Lang;
use sawaya_core::module::{BotModule, module_about_embed};
use sawaya_core::permissions::BotPermission;
use sawaya_core::async_trait;

use crate::debug::DebugCommand;

mod debug;

struct DebugModule;

#[async_trait]
impl BotModule for DebugModule {
    fn about(&self, lang: Arc<Lang>) -> EmbedData {
        module_about_embed(lang, self.codename())
            .color(0xd5aaff.into())
    }

    fn perm_level_to_add(&self) -> BotPermission {
        BotPermission::CREATOR
    }

    fn codename(&self) -> &str {
        "debug_module"
    }

    fn commands(&self) -> Vec<BoxedCommand> {
        vec![
            Box::new(DebugCommand)
        ]
    }
}

pub fn init() {
    sawaya_core::add_boxed_module(Box::new(DebugModule));
}