mod module;
mod language;
mod managers;

use sawaya_core::module::{BotModule, module_about_embed};
use sawaya_core::command::BoxedCommand;
use crate::module::ModuleCommand;
use crate::language::LanguageCommand;
use crate::managers::ManagersCommand;
use std::sync::Arc;
use sawaya_core::lang::Lang;
use sawaya_core::embed::EmbedData;
use sawaya_core::async_trait;

struct ConfiguratorModule;

#[async_trait]
impl BotModule for ConfiguratorModule {
    fn about(&self, lang: Arc<Lang>) -> EmbedData {
        module_about_embed(lang, self.codename())
            .color(0x85e3ff.into())
    }

    fn can_be_disabled(&self) -> bool {
        false
    }

    fn codename(&self) -> &str {
        "config_module"
    }

    fn commands(&self) -> Vec<BoxedCommand> {
        vec![
            Box::new(LanguageCommand),
            Box::new(ModuleCommand),
            Box::new(ManagersCommand)
        ]
    }
}

pub fn init() {
    sawaya_core::add_boxed_module(Box::new(ConfiguratorModule));
}