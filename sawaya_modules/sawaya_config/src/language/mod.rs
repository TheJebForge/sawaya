use sawaya_core::async_trait;
use sawaya_core::trn;
use sawaya_core::command::{BotCommand};
use sawaya_core::command::info::CommandInfo;
use sawaya_core::command::data::CommandData;
use sawaya_core::embed::EmbedData;
use sawaya_core::permissions::BotPermission;
use sawaya_core::command::parameters::{Parameter, ParameterType};
use sawaya_core::lang::{get_langs, get_lang};
use sawaya_core::command::arguments::Argument;

pub struct LanguageCommand;

#[async_trait]
impl BotCommand for LanguageCommand {
    fn info(&self) -> CommandInfo {
        CommandInfo::new(
            "language",
            "language"
        ).permission(
            BotPermission::MANAGERS
        ).parameters(
            &[
                Parameter::Optional(ParameterType::String)
            ]
        )
    }

    async fn run(&self, data: CommandData) -> Vec<EmbedData> {
        let selected_lang = &data.args[0];

        vec![
            {
                if let Argument::String(selected_lang) = selected_lang {
                    // Set
                    if get_langs().contains_key(&selected_lang.to_string().to_lowercase()) {
                        let guild_data = data.db.get_guild(&data.guild_id).await;

                        if let Some(_) = guild_data {
                            data.db.set_guild(&data.guild_id, selected_lang.to_lowercase()).await;

                            let lang = get_lang(&selected_lang.to_lowercase());

                            EmbedData::new()
                                .title(trn!(lang, "config.language.set.title"))
                                .desc(trn!(lang, "config.language.set.desc"))
                        } else {
                            EmbedData::new()
                                .title(trn!(data.lang, "error.no_guild_data.title"))
                                .desc(trn!(data.lang, "error.no_guild_data.desc"))
                        }
                    } else {
                        // Wrong language
                        let concat = get_langs().keys().map(|x| {
                            let mut r = x.to_string();

                            if r.len() > 0 {
                                r = format!("{}{}", &r[0..1].to_uppercase(), &r[1..]);
                            }

                            r
                        }).collect::<Vec<String>>().join(", ");

                        EmbedData::new()
                            .title(trn!(data.lang, "error.config.language.invalid.title", selected_lang))
                            .desc(trn!(data.lang, "error.config.language.invalid.desc", concat))
                            .failed()
                    }
                } else {
                    // Get
                    EmbedData::new()
                        .title(trn!(data.lang, "config.language.get.title"))
                        .desc(trn!(data.lang, "config.language.get.desc"))
                }
            }
        ]
    }
}