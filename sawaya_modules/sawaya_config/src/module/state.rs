use sawaya_core::{async_trait, get_modules};
use sawaya_core::trn;
use sawaya_core::command::BotCommand;
use sawaya_core::command::info::CommandInfo;
use sawaya_core::command::data::CommandData;
use sawaya_core::embed::EmbedData;
use sawaya_core::permissions::BotPermission;
use sawaya_core::command::parameters::{Parameter, ParameterType};
use sawaya_core::permissions::processor::{get_permission_access_level_from_data};

pub struct ModuleEnableCommand;

#[async_trait]
impl BotCommand for ModuleEnableCommand {
    fn info(&self) -> CommandInfo {
        CommandInfo::new(
            "module-enable",
            "enable"
        ).permission(
            BotPermission::MANAGERS
        ).parameters(
            &[
                Parameter::Required(ParameterType::String)
            ]
        )
    }

    async fn run(&self, data: CommandData) -> Vec<EmbedData> {
        let module_to_enable = &data.args[0].to_string();

        if data.db.get_enabled_module_names(&data.guild_id).await.contains(module_to_enable) {
            vec![
                EmbedData::new()
                    .failed()
                    .title(trn!(data.lang, "error.module.already_active.title", module_to_enable))
                    .desc(trn!(data.lang, "error.module.already_active.desc", module_to_enable))
            ]
        } else {
            let modules = get_modules();

            if let Some(module) = modules.get(module_to_enable) {
                let user_perm = get_permission_access_level_from_data(&data).await;
                let module_perm = module.perm_level_to_add();

                match user_perm.check(data.lang.clone(), &module_perm) {
                    Ok(_) => {
                        data.db.add_module(&data.guild_id, module_to_enable).await;

                        vec![
                            EmbedData::new()
                                .title(trn!(data.lang, "module.enabled.title", module_to_enable))
                                .desc(trn!(data.lang, "module.enabled.desc", module_to_enable))
                        ]
                    }

                    Err(embed) => {
                        vec![embed]
                    }
                }
            } else {
                vec![
                    EmbedData::new()
                        .failed()
                        .title(trn!(data.lang, "error.module.no_such_module.title", module_to_enable))
                        .desc(trn!(data.lang, "error.module.no_such_module.desc", module_to_enable))
                ]
            }
        }
    }
}

pub struct ModuleDisableCommand;

#[async_trait]
impl BotCommand for ModuleDisableCommand {
    fn info(&self) -> CommandInfo {
        CommandInfo::new(
            "module-disable",
            "disable"
        ).permission(
            BotPermission::MANAGERS
        ).parameters(
            &[
                Parameter::Required(ParameterType::String)
            ]
        )
    }

    async fn run(&self, data: CommandData) -> Vec<EmbedData> {
        let module_to_disable = &data.args[0].to_string();

        if !data.db.get_enabled_module_names(&data.guild_id).await.contains(module_to_disable) {
            vec![
                EmbedData::new()
                    .failed()
                    .title(trn!(data.lang, "error.module.already_disabled.title", module_to_disable))
                    .desc(trn!(data.lang, "error.module.already_disabled.desc", module_to_disable))
            ]
        } else {
            let modules = get_modules();

            if let Some(module) = modules.get(module_to_disable) {
                if module.can_be_disabled() {
                    data.db.remove_module(&data.guild_id, module_to_disable).await;

                    vec![
                        EmbedData::new()
                            .title(trn!(data.lang, "module.disabled.title", module_to_disable))
                            .desc(trn!(data.lang, "module.disabled.desc", module_to_disable))
                    ]
                } else {
                    vec![
                        EmbedData::new()
                            .failed()
                            .title(trn!(data.lang, "error.module.cannot_disable.title", module_to_disable))
                            .desc(trn!(data.lang, "error.module.cannot_disable.desc", module_to_disable))
                    ]
                }
            } else {
                vec![
                    EmbedData::new()
                        .failed()
                        .title(trn!(data.lang, "error.module.no_such_module.title", module_to_disable))
                        .desc(trn!(data.lang, "error.module.no_such_module.desc", module_to_disable))
                ]
            }
        }
    }
}