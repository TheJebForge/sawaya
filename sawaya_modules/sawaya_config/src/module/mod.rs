mod active;
mod list;
mod state;

use sawaya_core::async_trait;
use sawaya_core::command::{BotCommand, no_run};
use sawaya_core::command::info::CommandInfo;
use sawaya_core::command::data::CommandData;
use sawaya_core::embed::EmbedData;
use sawaya_core::permissions::BotPermission;
use crate::module::active::ModuleActiveCommand;
use crate::module::list::ModuleListCommand;
use crate::module::state::{ModuleEnableCommand, ModuleDisableCommand};

pub struct ModuleCommand;

#[async_trait]
impl BotCommand for ModuleCommand {
    fn info(&self) -> CommandInfo {
        CommandInfo::new(
            "module",
            "module"
        ).permission(
            BotPermission::ANYONE
        ).children(
            vec![
                Box::new(ModuleActiveCommand),
                Box::new(ModuleListCommand),
                Box::new(ModuleEnableCommand),
                Box::new(ModuleDisableCommand)
            ]
        )
    }

    async fn run(&self, data: CommandData) -> Vec<EmbedData> {
        no_run(data)
    }
}