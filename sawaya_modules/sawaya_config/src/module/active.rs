use sawaya_core::async_trait;
use sawaya_core::command::BotCommand;
use sawaya_core::command::info::CommandInfo;
use sawaya_core::command::data::CommandData;
use sawaya_core::embed::EmbedData;
use sawaya_core::permissions::BotPermission;
use sawaya_core::trn;

pub struct ModuleActiveCommand;

#[async_trait]
impl BotCommand for ModuleActiveCommand {
    fn info(&self) -> CommandInfo {
        CommandInfo::new(
            "module-active",
            "active"
        ).permission(
            BotPermission::ANYONE
        )
    }

    async fn run(&self, data: CommandData) -> Vec<EmbedData> {
        let modules = data.db.get_enabled_modules(&data.guild_id).await;

        let mut embeds = vec![
            EmbedData::new()
                .title(trn!(data.lang, "module.active.title"))
        ];

        for module in modules {
            embeds.push(module.about(data.lang.clone()))
        }

        embeds
    }
}