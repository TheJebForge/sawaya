use sawaya_core::{async_trait, get_modules};
use sawaya_core::trn;
use sawaya_core::command::BotCommand;
use sawaya_core::command::info::CommandInfo;
use sawaya_core::command::data::CommandData;
use sawaya_core::embed::EmbedData;
use sawaya_core::permissions::BotPermission;

pub struct ModuleListCommand;

#[async_trait]
impl BotCommand for ModuleListCommand {
    fn info(&self) -> CommandInfo {
        CommandInfo::new(
            "module-list",
            "list"
        ).permission(
            BotPermission::ANYONE
        )
    }

    async fn run(&self, data: CommandData) -> Vec<EmbedData> {
        let modules = get_modules();

        let mut embeds = vec![
            EmbedData::new()
                .title(trn!(data.lang, "module.list.title"))
        ];

        let enabled = data.db.get_enabled_module_names(&data.guild_id).await;

        let module_embeds = modules.values()
            .filter(|x| !enabled.contains(&x.codename().to_string()))
            .map(|x| x.about(data.lang.clone()))
            .collect::<Vec<EmbedData>>();

        embeds.extend(module_embeds);

        embeds
    }
}