mod change;
mod list;

use sawaya_core::async_trait;
use sawaya_core::command::{BotCommand, no_run};
use sawaya_core::command::info::CommandInfo;
use sawaya_core::command::data::CommandData;
use sawaya_core::embed::EmbedData;
use sawaya_core::permissions::BotPermission;
use crate::managers::list::ManagersListCommand;
use crate::managers::change::{ManagersAddCommand, ManagersRemoveCommand};

pub struct ManagersCommand;

#[async_trait]
impl BotCommand for ManagersCommand {
    fn info(&self) -> CommandInfo {
        CommandInfo::new(
            "managers",
            "managers"
        ).permission(
            BotPermission::ANYONE
        ).children(
            vec![
                Box::new(ManagersListCommand),
                Box::new(ManagersAddCommand),
                Box::new(ManagersRemoveCommand)
            ]
        )
    }

    async fn run(&self, data: CommandData) -> Vec<EmbedData> {
        no_run(data)
    }
}