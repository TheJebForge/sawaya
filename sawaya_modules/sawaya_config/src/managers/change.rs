use sawaya_core::async_trait;
use sawaya_core::trn;
use sawaya_core::command::BotCommand;
use sawaya_core::command::info::CommandInfo;
use sawaya_core::command::data::CommandData;
use sawaya_core::embed::EmbedData;
use sawaya_core::permissions::BotPermission;
use sawaya_core::command::parameters::{Parameter, ParameterType};

pub struct ManagersAddCommand;

#[async_trait]
impl BotCommand for ManagersAddCommand {
    fn info(&self) -> CommandInfo {
        CommandInfo::new(
            "managers-add",
            "add"
        ).permission(
            BotPermission::MANAGERS
        ).parameters(
            &[
                Parameter::Required(ParameterType::Mention)
            ]
        )
    }

    async fn run(&self, data: CommandData) -> Vec<EmbedData> {
        let user_id = data.args[0].to_user().id.0.to_string();

        if data.db.get_managers(&data.guild_id).await.contains(&user_id) {
            vec![
                EmbedData::new()
                    .title(trn!(data.lang, "error.manager.already.title"))
                    .desc(trn!(data.lang, "error.manager.already.desc"))
                    .failed()
            ]
        } else {
            data.db.add_manager(&data.guild_id, &user_id).await;

            vec![
                EmbedData::new()
                    .title(trn!(data.lang, "manager.added.title"))
                    .desc(trn!(data.lang, "manager.added.desc"))
            ]
        }
    }
}

pub struct ManagersRemoveCommand;

#[async_trait]
impl BotCommand for ManagersRemoveCommand {
    fn info(&self) -> CommandInfo {
        CommandInfo::new(
            "managers-remove",
            "remove"
        ).permission(
            BotPermission::MANAGERS
        ).parameters(
            &[
                Parameter::Required(ParameterType::Mention)
            ]
        )
    }

    async fn run(&self, data: CommandData) -> Vec<EmbedData> {
        let user_id = data.args[0].to_user().id.0.to_string();

        if !data.db.get_managers(&data.guild_id).await.contains(&user_id) {
            vec![
                EmbedData::new()
                    .title(trn!(data.lang, "error.manager.already_not.title"))
                    .desc(trn!(data.lang, "error.manager.already_not.desc"))
                    .failed()
            ]
        } else {
            if user_id != data.sensitive.creator_id {
                data.db.remove_manager(&data.guild_id, &user_id).await;

                vec![
                    EmbedData::new()
                        .title(trn!(data.lang, "manager.removed.title"))
                        .desc(trn!(data.lang, "manager.removed.desc"))
                ]
            } else {
                vec![
                    EmbedData::new()
                        .title(trn!(data.lang, "error.manager.removing.creator.title"))
                        .desc(trn!(data.lang, "error.manager.removing.creator.desc"))
                        .failed()
                ]
            }
        }
    }
}