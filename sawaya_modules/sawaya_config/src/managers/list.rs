use sawaya_core::async_trait;
use sawaya_core::trn;
use sawaya_core::command::BotCommand;
use sawaya_core::command::info::CommandInfo;
use sawaya_core::command::data::CommandData;
use sawaya_core::embed::EmbedData;
use sawaya_core::permissions::BotPermission;
use sawaya_core::serenity::model::id::UserId;
use std::ops::Deref;

pub struct ManagersListCommand;

#[async_trait]
impl BotCommand for ManagersListCommand {
    fn info(&self) -> CommandInfo {
        CommandInfo::new(
            "managers-list",
            "list"
        ).permission(
            BotPermission::ANYONE
        )
    }

    async fn run(&self, data: CommandData) -> Vec<EmbedData> {
        let managers = data.db.get_managers(&data.guild_id).await;

        let mut fetched_managers = vec![];
        for manager in managers {
            if let Ok(id) = manager.parse::<u64>() {
                if let Some(guild_id) = data.message.guild_id {
                    if let Ok(member) = guild_id.member(data.ctx.deref(), id).await {
                        if let Some(nick) = member.nick {
                            fetched_managers.push(nick);
                        } else {
                            fetched_managers.push(format!("{}#{:0>4}", member.user.name, member.user.discriminator));
                        }
                    }
                } else if let Ok(user) = UserId(id).to_user(data.ctx.deref()).await {
                    fetched_managers.push(format!("{}#{:0>4}", user.name, user.discriminator));
                }
            }
        }

        vec![
            EmbedData::new()
                .title(trn!(data.lang, "managers.list.title"))
                .desc(fetched_managers.join("\n"))
        ]
    }
}