use sawaya_core::async_trait;
use sawaya_core::command::chain::{ChainCommand, ChainCommandInfo};
use sawaya_core::command::data::CommandData;
use sawaya_core::command::parameters::{Parameter, ParameterType};
use sawaya_core::embed::EmbedData;
use crate::mover::{get_channel, MoveAction};

pub struct MoverFromCommand;

#[async_trait]
impl ChainCommand<MoveAction> for MoverFromCommand {
    fn info(&self) -> ChainCommandInfo {
        ChainCommandInfo::new(
            "mover-from",
            "from"
        ).parameters(
            &[
                Parameter::Required(ParameterType::String)
            ]
        )
    }

    async fn run(&self, shared: &mut MoveAction, data: CommandData) -> Result<(), EmbedData> {
        let arg = data.args[0].to_string();

        match get_channel(arg, data.clone()).await {
            Ok(channel) => {
                shared.from = Some(channel);
                Ok(())
            }

            Err(error) => Err(error)
        }
    }
}