use std::ops::Deref;
use sawaya_core::async_trait;
use sawaya_core::trn;
use sawaya_core::command::chain::{ChainCommand, ChainCommandInfo};
use sawaya_core::command::data::CommandData;
use sawaya_core::command::parameters::{Parameter, ParameterType};
use sawaya_core::embed::EmbedData;
use sawaya_core::serenity::model::id::UserId;
use crate::mover::MoveAction;

pub struct MoverIncludeCommand;

#[async_trait]
impl ChainCommand<MoveAction> for MoverIncludeCommand {
    fn info(&self) -> ChainCommandInfo {
        ChainCommandInfo::new(
            "mover-include",
            "include"
        ).parameters(
            &[
                Parameter::Required(ParameterType::String)
            ]
        )
    }

    async fn run(&self, shared: &mut MoveAction, data: CommandData) -> Result<(), EmbedData> {
        let arg = data.args[0].to_string();

        if let Ok(id) = arg.parse::<u64>() {
            let user_id = UserId(id);

            if let Ok(_) = user_id.to_user(data.ctx.deref()).await {
                if let Some(inclusions) = shared.inclusions.as_mut() {
                    inclusions.push(user_id);
                } else {
                    shared.inclusions = Some(vec![user_id]);
                }

                Ok(())
            } else {
                Err(
                    EmbedData::new()
                        .title(trn!(data.lang, "error.invalid_user_id.title"))
                        .desc(trn!(data.lang, "error.invalid_user_id.desc", arg))
                        .failed()
                )
            }
        } else {
            Err(
                EmbedData::new()
                    .title(trn!(data.lang, "error.invalid_user_id.title"))
                    .desc(trn!(data.lang, "error.invalid_user_id.desc", arg))
                    .failed()
            )
        }
    }
}