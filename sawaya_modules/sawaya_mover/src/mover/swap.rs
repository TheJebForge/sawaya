use sawaya_core::async_trait;
use sawaya_core::command::chain::{ChainCommand, ChainCommandInfo};
use sawaya_core::command::data::CommandData;
use sawaya_core::embed::EmbedData;
use crate::mover::MoveAction;

pub struct MoverSwapCommand;

#[async_trait]
impl ChainCommand<MoveAction> for MoverSwapCommand {
    fn info(&self) -> ChainCommandInfo {
        ChainCommandInfo::new(
            "mover-swap",
            "swap"
        )
    }

    async fn run(&self, shared: &mut MoveAction, _: CommandData) -> Result<(), EmbedData> {
        shared.swap = true;
        Ok(())
    }
}