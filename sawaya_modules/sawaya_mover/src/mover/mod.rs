mod from;
mod to;
mod exclude;
mod include;
mod swap;

use std::ops::Deref;
use sawaya_core::async_trait;
use sawaya_core::trn;
use sawaya_core::command::BotCommand;
use sawaya_core::command::chain::process_chain;
use sawaya_core::command::data::CommandData;
use sawaya_core::command::info::CommandInfo;
use sawaya_core::embed::EmbedData;
use sawaya_core::permissions::BotPermission;
use sawaya_core::serenity::model::channel::Channel;
use sawaya_core::serenity::model::id::{ChannelId, UserId};
use crate::mover::exclude::MoverExcludeCommand;
use crate::mover::from::MoverFromCommand;
use crate::mover::include::MoverIncludeCommand;
use crate::mover::swap::MoverSwapCommand;
use crate::mover::to::MoverToCommand;

pub struct MoverCommand;

#[async_trait]
impl BotCommand for MoverCommand {
    fn info(&self) -> CommandInfo {
        CommandInfo::new(
            "mover",
            "mover"
        ).permission(
            BotPermission::MANAGERS
        )
    }

    async fn run(&self, data: CommandData) -> Vec<EmbedData> {
        let result = process_chain(
            MoveAction {
                from: None,
                to: None,
                exclusions: vec![],
                inclusions: None,
                swap: false
            },
            vec![
                Box::new(MoverFromCommand),
                Box::new(MoverToCommand),
                Box::new(MoverIncludeCommand),
                Box::new(MoverExcludeCommand),
                Box::new(MoverSwapCommand),
            ],
            data.clone()
        ).await;

        match result {
            Ok(action) => {
                if let Some(from) = action.from {
                    if let Some(to) = action.to {
                        let from_channel = from.to_channel(data.ctx.deref()).await.unwrap();

                        if let Channel::Guild(guild) = from_channel {
                            if let Ok(members) = guild.members(data.ctx.deref()).await {
                                let mut count = 0;

                                if action.swap {
                                    if let Some(guild) = data.message.guild(data.ctx.deref()).await {
                                        let states = {
                                            let mut vec = vec![];

                                            for (id, state) in guild.voice_states.clone() {
                                                vec.push((id.clone(), guild.member(data.ctx.deref(), id).await.ok(), state.clone()));
                                            }

                                            vec
                                        };

                                        for (id, member, state) in &states {
                                            if let Some(member) = member {
                                                if !action.exclusions.contains(&id) {
                                                    if let Some(inclusions) = &action.inclusions {
                                                        if inclusions.contains(&id) {
                                                            if let Some(channel) = state.channel_id {
                                                                if channel == from {
                                                                    if let Ok(_) = member.move_to_voice_channel(data.ctx.deref(), to).await {
                                                                        count += 1;
                                                                    }
                                                                } else if channel == to {
                                                                    if let Ok(_) = member.move_to_voice_channel(data.ctx.deref(), from).await {
                                                                        count += 1;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    } else {
                                                        if let Some(channel) = state.channel_id {
                                                            if channel == from {
                                                                if let Ok(_) = member.move_to_voice_channel(data.ctx.deref(), to).await {
                                                                    count += 1;
                                                                }
                                                            } else if channel == to {
                                                                if let Ok(_) = member.move_to_voice_channel(data.ctx.deref(), from).await {
                                                                    count += 1;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    for member in members {
                                        if !action.exclusions.contains(&member.user.id) {
                                            if let Some(inclusions) = &action.inclusions {
                                                if inclusions.contains(&member.user.id) {
                                                    if let Ok(_) = member.move_to_voice_channel(data.ctx.deref(), to).await {
                                                        count += 1;
                                                    }
                                                }
                                            } else {
                                                if let Ok(_) = member.move_to_voice_channel(data.ctx.deref(), to).await {
                                                    count += 1;
                                                }
                                            }
                                        }
                                    }
                                }

                                vec![
                                    EmbedData::new()
                                        .title(trn!(data.lang, "mover.moved.title", count))
                                        .desc(trn!(data.lang, "mover.moved.desc", count))
                                ]
                            } else {
                                vec![
                                    EmbedData::new()
                                        .title(trn!(data.lang, "error.failed_fetch_users.title"))
                                        .desc(trn!(data.lang, "error.failed_fetch_users.desc"))
                                        .failed()
                                ]
                            }
                        } else {
                            vec![
                                EmbedData::new()
                                    .title(trn!(data.lang, "error.unsupported_channel.title"))
                                    .desc(trn!(data.lang, "error.unsupported_channel.desc"))
                                    .failed()
                            ]
                        }
                    } else {
                        vec![
                            EmbedData::new()
                                .title(trn!(data.lang, "error.mover.no_to.title"))
                                .desc(trn!(data.lang, "error.mover.no_to.desc"))
                                .failed()
                        ]
                    }
                } else {
                    vec![
                        EmbedData::new()
                            .title(trn!(data.lang, "error.mover.no_from.title"))
                            .desc(trn!(data.lang, "error.mover.no_from.desc"))
                            .failed()
                    ]
                }
            }

            Err(err) => vec![
                err
            ]
        }
    }
}

pub struct MoveAction {
    pub from: Option<ChannelId>,
    pub to: Option<ChannelId>,
    pub exclusions: Vec<UserId>,
    pub inclusions: Option<Vec<UserId>>,
    pub swap: bool
}

pub async fn get_channel(arg: String, data: CommandData) -> Result<ChannelId, EmbedData> {
    if arg == "current" {
        if let Some(guild) = data.message.guild(data.ctx).await {
            if let Some(state) = guild.voice_states.get(&data.message.author.id) {
                if let Some(channel) = state.channel_id {
                    return Ok(channel);
                }
            }
        }

        Err(
            EmbedData::new()
                .title(trn!(data.lang, "error.mover.no_current.title"))
                .desc(trn!(data.lang, "error.mover.no_current.desc"))
                .failed()
        )
    } else if let Ok(id) = arg.parse::<u64>() {
        let channel = ChannelId(id);

        if let Ok(_) = channel.to_channel(data.ctx.deref()).await {
            Ok(channel)
        } else {
            Err(
                EmbedData::new()
                    .title(trn!(data.lang, "error.invalid_channel_id.title", arg))
                    .desc(trn!(data.lang, "error.invalid_channel_id.desc", arg))
                    .failed()
            )
        }
    } else {
        Err(
            EmbedData::new()
                .title(trn!(data.lang, "error.invalid_channel_id.title", arg))
                .desc(trn!(data.lang, "error.invalid_channel_id.desc", arg))
                .failed()
        )
    }
}