use sawaya_core::async_trait;
use sawaya_core::command::chain::{ChainCommand, ChainCommandInfo};
use sawaya_core::command::data::CommandData;
use sawaya_core::command::parameters::{Parameter, ParameterType};
use sawaya_core::embed::EmbedData;
use crate::mover::{get_channel, MoveAction};

pub struct MoverToCommand;

#[async_trait]
impl ChainCommand<MoveAction> for MoverToCommand {
    fn info(&self) -> ChainCommandInfo {
        ChainCommandInfo::new(
            "mover-to",
            "to"
        ).parameters(
            &[
                Parameter::Required(ParameterType::String)
            ]
        )
    }

    async fn run(&self, shared: &mut MoveAction, data: CommandData) -> Result<(), EmbedData> {
        let arg = data.args[0].to_string();

        match get_channel(arg, data.clone()).await {
            Ok(channel) => {
                shared.to = Some(channel);
                Ok(())
            }

            Err(error) => Err(error)
        }
    }
}