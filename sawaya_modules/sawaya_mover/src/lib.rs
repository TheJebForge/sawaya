mod mover;

use std::sync::Arc;
use sawaya_core::async_trait;
use sawaya_core::command::BoxedCommand;
use sawaya_core::embed::EmbedData;
use sawaya_core::lang::Lang;
use sawaya_core::module::{BotModule, module_about_embed};
use crate::mover::MoverCommand;

pub struct MoverModule;

#[async_trait]
impl BotModule for MoverModule {
    fn about(&self, lang: Arc<Lang>) -> EmbedData {
        module_about_embed(lang, self.codename())
            .color(0xace7ff.into())
    }

    fn codename(&self) -> &str {
        "mover_module"
    }

    fn commands(&self) -> Vec<BoxedCommand> {
        vec![
            Box::new(MoverCommand)
        ]
    }
}

pub fn init() {
    sawaya_core::add_boxed_module(Box::new(MoverModule));
}