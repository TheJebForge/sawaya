use std::str::FromStr;
use regex::Regex;
use tokio_postgres::Row;
use std::fmt::{Display, Formatter};

#[derive(Debug, Clone)]
pub enum DailyTopType {
    Points,
    GuildPoints(String),
    Levels,
    GuildLevels(String),
    Time,
    GuildTime(String),
    PrayStreak,
    GuildPrayStreak(String),
    Broken,
}

#[allow(dead_code)]
impl DailyTopType {
    pub fn guild_id(&self) -> Option<String> {
        match self {
            DailyTopType::GuildPoints(id) => Some(id.to_string()),
            DailyTopType::GuildLevels(id) => Some(id.to_string()),
            DailyTopType::GuildTime(id) => Some(id.to_string()),
            DailyTopType::GuildPrayStreak(id) => Some(id.to_string()),
            _ => None
        }
    }

    pub fn base_type(&self) -> DailyTopType {
        match self {
            DailyTopType::Points | DailyTopType::GuildPoints(_) => DailyTopType::Points,
            DailyTopType::Levels | DailyTopType::GuildLevels(_) => DailyTopType::Levels,
            DailyTopType::Time | DailyTopType::GuildTime(_) => DailyTopType::Time,
            DailyTopType::PrayStreak | DailyTopType::GuildPrayStreak(_) => DailyTopType::PrayStreak,
            _ => DailyTopType::Broken,
        }
    }
}

impl Display for DailyTopType {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl FromStr for DailyTopType {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut ty = s;
        let mut id = "";

        let regex = Regex::new(r#"(.*)\("(.*)""#).unwrap();

        if let Some(captures) = regex.captures(s) {
            if let Some(parsed_ty) = captures.get(1) {
                ty = parsed_ty.as_str();

                if let Some(parsed_id) = captures.get(2) {
                    id = parsed_id.as_str();
                }
            }
        }

        match ty {
            "Points" => Ok(DailyTopType::Points),
            "GuildPoints" => Ok(DailyTopType::GuildPoints(id.to_string())),
            "Levels" => Ok(DailyTopType::Levels),
            "GuildLevels" => Ok(DailyTopType::GuildLevels(id.to_string())),
            "Time" => Ok(DailyTopType::Time),
            "GuildTime" => Ok(DailyTopType::GuildTime(id.to_string())),
            "PrayStreak" => Ok(DailyTopType::PrayStreak),
            "GuildPrayStreak" => Ok(DailyTopType::GuildPrayStreak(id.to_string())),
            _ => Err(())
        }
    }
}

#[derive(Debug, Clone)]
pub struct DailyTop {
    pub message_id: String,
    pub channel_id: String,
    pub top_type: DailyTopType,
}

impl From<&Row> for DailyTop {
    fn from(row: &Row) -> Self {
        DailyTop {
            message_id: row.get("message_id"),
            channel_id: row.get("channel_id"),
            top_type: row.get::<&str, String>("top_type").parse().unwrap_or(DailyTopType::Broken)
        }
    }
}
