use crate::db::get_daily_tops;
use sawaya_core::db::DB;
use std::sync::Arc;
use sawaya_core::serenity::client::Context;
use sawaya_core::serenity::model::id::{ChannelId, UserId};
use std::ops::Deref;
use crate::model::{DailyTopType};
use sawaya_core::serenity::builder::CreateEmbed;
use sawaya_core::vp::VoiceProfileClient;
use sawaya_core::embed::EmbedData;
use sawaya_core::lang::get_lang;
use sawaya_core::trn;
use sawaya_core::processor::embed::process_embed;
use sawaya_core::serenity::model::channel::Channel;

pub async fn update_dailytops(ctx: Arc<Context>, db: Arc<DB>, vp: Arc<VoiceProfileClient>) {
    log::info!("Updating daily tops...");

    let tops = get_daily_tops(db.clone()).await;
    let mut count = 0;

    for top in &tops {
        if let Ok(channel_id) = top.channel_id.parse::<u64>() {
            if let Ok(channel) = ChannelId(channel_id).to_channel(ctx.deref()).await {
                if let Ok(message_id) = top.message_id.parse::<u64>() {
                    match channel {
                        Channel::Guild(channel) => {
                            if let Some(embed) = top_embed(top.top_type.clone(), vp.clone(), ctx.clone(), db.clone()).await {
                                if let Ok(_) = channel.edit_message(ctx.deref(), message_id, |m| {
                                    m.set_embed(embed)
                                }).await {
                                    count += 1;
                                }
                            }
                        }
                        Channel::Private(channel) => {
                            if let Some(embed) = top_embed(top.top_type.clone(), vp.clone(), ctx.clone(), db.clone()).await {
                                if let Ok(_) = channel.edit_message(ctx.deref(), message_id, |m| {
                                    m.set_embed(embed)
                                }).await {
                                    count += 1;
                                }
                            }
                        }
                        _ => {}
                    }
                } else {
                    log::error!("Couldn't parse message_id: {}", top.message_id);
                }
            } else {
                log::error!("Failed to fetch channel");
            }
        } else {
            log::error!("Couldn't parse channel_id: {}", top.channel_id);
        }
    }

    log::info!("Updated {} daily tops", count);
}

pub async fn top_embed(ty: DailyTopType, vp: Arc<VoiceProfileClient>, ctx: Arc<Context>, db: Arc<DB>) -> Option<CreateEmbed> {
    let lang = {
        let def = get_lang("english");

        if let Some(guild_id) = ty.guild_id() {
            if let Some(data) = db.get_guild(&guild_id).await {
                get_lang(&data.language)
            } else { def }
        } else { def }
    };

    match ty.base_type() {
        DailyTopType::Points => {
            let profiles = {
                vp.get_top(ty.guild_id().as_deref(), "points").await
            };

            if let Ok(profiles) = profiles {
                let mut embed = EmbedData::new()
                    .title(trn!(lang, "dailytop.top.title"))
                    .desc(trn!(lang, "dailytop.top.points_desc"))
                    .timestamp_to_current();

                for (i, profile) in profiles.iter().enumerate() {
                    if let Ok(id) = profile.user_id.parse::<u64>() {
                        if let Ok(user) = UserId(id).to_user(ctx.deref()).await {
                            embed.add_field(
                                &format!("#{} {}#{:0>4}", i + 1, user.name, user.discriminator),
                                &format!("{} VP", profile.voicepoints),
                                false
                            );
                        }
                    }
                }

                Some(process_embed(&embed))
            } else {
                None
            }
        }

        DailyTopType::Levels => {
            let profiles = {
                vp.get_top(ty.guild_id().as_deref(), "levels").await
            };

            if let Ok(profiles) = profiles {
                let mut embed = EmbedData::new()
                    .title(trn!(lang, "dailytop.top.title"))
                    .desc(trn!(lang, "dailytop.top.levels_desc"))
                    .timestamp_to_current();

                for (i, profile) in profiles.iter().enumerate() {
                    if let Ok(id) = profile.user_id.parse::<u64>() {
                        if let Ok(user) = UserId(id).to_user(ctx.deref()).await {
                            let next_level = (10 + profile.level) * 10 * profile.level * profile.level;
                            let percentage = f64::floor(profile.experience as f64 / next_level as f64 * 100.0) as i64;

                            embed.add_field(
                                &format!("#{} {}#{:0>4}", i + 1, user.name, user.discriminator),
                                &trn!(lang, "dailytop.top.levels_format", profile.level, profile.experience, percentage),
                                false
                            );
                        }
                    }
                }

                Some(process_embed(&embed))
            } else {
                None
            }
        }

        DailyTopType::Time => {
            let profiles = {
                vp.get_top(ty.guild_id().as_deref(), "time").await
            };

            if let Ok(profiles) = profiles {
                let mut embed = EmbedData::new()
                    .title(trn!(lang, "dailytop.top.title"))
                    .desc(trn!(lang, "dailytop.top.time_desc"))
                    .timestamp_to_current();

                for (i, profile) in profiles.iter().enumerate() {
                    if let Ok(id) = profile.user_id.parse::<u64>() {
                        if let Ok(user) = UserId(id).to_user(ctx.deref()).await {
                            if let Some(global) = profile.timespent.get("global") {
                                let mut global = *global;

                                let days = global / (3600 * 24);
                                global %= 60 * 60 * 24;

                                let hours = format!("{:0>2}", global / 3600);
                                global %= 3600;

                                let minutes = format!("{:0>2}", global / 60);

                                let seconds = format!("{:0>2}", global % 60);

                                embed.add_field(
                                    &format!("#{} {}#{:0>4}", i + 1, user.name, user.discriminator),
                                    &trn!(lang, "dailytop.top.time_format", days, hours, minutes, seconds),
                                    false
                                );
                            }
                        }
                    }
                }

                Some(process_embed(&embed))
            } else {
                None
            }
        }

        DailyTopType::PrayStreak => {
            let profiles = {
                vp.get_top(ty.guild_id().as_deref(), "pray-streak").await
            };

            if let Ok(profiles) = profiles {
                let mut embed = EmbedData::new()
                    .title(trn!(lang, "dailytop.top.title"))
                    .desc(trn!(lang, "dailytop.top.pray_streak_desc"))
                    .timestamp_to_current();

                for (i, profile) in profiles.iter().enumerate() {
                    if let Ok(id) = profile.user_id.parse::<u64>() {
                        if let Ok(user) = UserId(id).to_user(ctx.deref()).await {
                            embed.add_field(
                                &format!("#{} {}#{:0>4}", i + 1, user.name, user.discriminator),
                                &trn!(lang, "dailytop.top.pray_streak_format", profile.pray.streak),
                                false
                            );
                        }
                    }
                }

                Some(process_embed(&embed))
            } else {
                None
            }
        }

        _ => panic!("Unthinkable happened"),
    }
}