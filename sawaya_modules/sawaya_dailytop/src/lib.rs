use sawaya_core::command::BoxedCommand;
use sawaya_core::module::{BotModule, module_about_embed};
use crate::dailytop::DailyTopCommand;
use std::sync::Arc;
use sawaya_core::lang::Lang;
use sawaya_core::embed::EmbedData;
use crate::task::update_dailytops;
use std::time::Duration;
use std::thread;
use sawaya_core::db::DB;
use tokio::time::Instant;
use sawaya_core::serenity::client::Context;
use sawaya_core::config::SensitiveConfig;
use sawaya_core::vp::VoiceProfileClient;
use tokio::runtime::Handle;
use sawaya_core::async_trait;

mod db;
mod model;
mod dailytop;
mod task;

struct DailyTopModule;

#[async_trait]
impl BotModule for DailyTopModule {
    fn init(&self, ctx: Arc<Context>, db: Arc<DB>) {
        log::info!("Spawning update task in separate thread");

        let handle = Handle::current();

        thread::spawn(move || {
            let ctx = ctx;
            let db = db;
            let vp = VoiceProfileClient::init(Arc::new(SensitiveConfig::get()));
            let mut last_time = Instant::now();

            loop {
                // Interval adjusted here
                if last_time.elapsed().as_secs() > 600 {
                    handle.spawn(update_dailytops(ctx.clone(), db.clone(), vp.clone()));
                    last_time = Instant::now();
                }

                thread::sleep(Duration::from_millis(10000));
            }
        });
    }

    fn about(&self, lang: Arc<Lang>) -> EmbedData {
        module_about_embed(lang, self.codename())
            .color(0x6eb5ff.into())
    }

    fn codename(&self) -> &str {
        "dailytop_module"
    }

    fn commands(&self) -> Vec<BoxedCommand> {
        vec![
            Box::new(DailyTopCommand)
        ]
    }
}

pub fn init() {
    sawaya_core::add_boxed_module(Box::new(DailyTopModule));
}