use sawaya_core::db::{DB, process_result};
use std::sync::Arc;
use crate::model::DailyTop;
use sawaya_core::serenity::client::Context;
use sawaya_core::serenity::model::id::ChannelId;
use std::ops::Deref;
use sawaya_core::processor::embed::process_embed;
use sawaya_core::embed::EmbedData;
use sawaya_core::lang::Lang;
use sawaya_core::trn;

#[allow(dead_code)]
pub async fn get_daily_tops(db: Arc<DB>) -> Vec<DailyTop> {
    let rows = db.0.query("select * from bot.dailytop", &[]).await;

    let mut tops = vec![];

    match rows {
        Ok(rows) => {
            for row in rows {
                tops.push(DailyTop::from(&row));
            }
        }
        Err(error) => {
            log::error!("Failed to get enabled modules: {:?}", error);
        },
    }

    tops
}

#[allow(dead_code)]
pub async fn daily_top_exists(db: Arc<DB>, top: &DailyTop) -> bool {
    get_daily_tops(db.clone()).await.into_iter().filter(|x| x.message_id == top.message_id).collect::<Vec<DailyTop>>().len() > 0
}

#[allow(dead_code)]
pub async fn add_daily_top(db: Arc<DB>, top: &DailyTop) {
    if !daily_top_exists(db.clone(), top).await {
        process_result(db.0.execute("insert into bot.dailytop (message_id, channel_id, top_type) values \
            ($1, $2, $3);", &[&top.message_id, &top.channel_id, &top.top_type.to_string()]).await);
    }
}

#[allow(dead_code)]
pub async fn remove_daily_top(db: Arc<DB>, top: &DailyTop) {
    if daily_top_exists(db.clone(), top).await {
        process_result(db.0.execute("delete from bot.dailytop \
            where message_id = $1;", &[&top.message_id]).await);
    }
}

#[allow(dead_code)]
pub async fn remove_daily_tops_by_channel(ctx: Arc<Context>, db: Arc<DB>, lang: Arc<Lang>, channel_id: &str) {
    let daily_tops = get_daily_tops(db.clone()).await.into_iter().filter(|x| x.channel_id == channel_id).collect::<Vec<DailyTop>>();

    if daily_tops.len() > 0 {
        if let Ok(id) = channel_id.parse::<u64>() {
            let channel = ChannelId(id);

            for top in daily_tops {
                if let Ok(id) = top.message_id.parse::<u64>() {
                    channel.edit_message(ctx.deref(), id, |m| {
                        m.set_embed(
                            process_embed(
                                &EmbedData::new()
                                    .title(trn!(lang, "dailytop.deleted.title"))
                                    .desc(trn!(lang, "dailytop.deleted.desc"))
                            )
                        )
                    }).await.ok();
                }

            }

            process_result(db.0.execute("delete from bot.dailytop \
                where channel_id = $1;", &[&channel_id.to_string()]).await);
        }
    }
}