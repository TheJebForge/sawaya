use sawaya_core::async_trait;
use sawaya_core::trn;
use sawaya_core::command::BotCommand;
use sawaya_core::command::info::CommandInfo;
use sawaya_core::command::data::CommandData;
use sawaya_core::embed::EmbedData;
use sawaya_core::permissions::BotPermission;
use sawaya_core::command::parameters::{Parameter, ParameterType};
use std::collections::HashMap;
use crate::model::{DailyTopType, DailyTop};
use std::ops::Deref;
use sawaya_core::processor::embed::process_embed;
use crate::db::add_daily_top;
use sawaya_core::command::arguments::Argument;

pub struct DailyTopCreateCommand;

#[async_trait]
impl BotCommand for DailyTopCreateCommand {
    fn info(&self) -> CommandInfo {
        CommandInfo::new(
            "dailytop-create",
            "create"
        ).permission(
            BotPermission::MANAGERS
        ).parameters(
            &[
                Parameter::Required(ParameterType::String),
                Parameter::Optional(ParameterType::String)
            ]
        )
    }

    async fn run(&self, data: CommandData) -> Vec<EmbedData> {
        let guild_id = {
            if let Argument::String(id) = &data.args[1] {
                id.to_string()
            } else {
                data.guild_id.to_string()
            }
        };

        let mut types = HashMap::new();

        types.insert("points".to_string(), DailyTopType::Points);
        types.insert("levels".to_string(), DailyTopType::Levels);
        types.insert("time".to_string(), DailyTopType::Time);
        types.insert("praystreak".to_string(), DailyTopType::PrayStreak);
        types.insert("guildpoints".to_string(), DailyTopType::GuildPoints(guild_id.to_string()));
        types.insert("guildlevels".to_string(), DailyTopType::GuildLevels(guild_id.to_string()));
        types.insert("guildtime".to_string(), DailyTopType::GuildTime(guild_id.to_string()));
        types.insert("guildpraystreak".to_string(), DailyTopType::GuildPrayStreak(guild_id.to_string()));

        if let Some(ty) = types.get(&data.args[0].to_string()) {
            if let Ok(message) = data.message.channel_id.send_message(data.ctx.deref(), |m| {
                m.set_embed(process_embed(
                    &EmbedData::new()
                        .title(trn!(data.lang, "dailytop.new.title"))
                        .desc(trn!(data.lang, "dailytop.new.desc"))
                ))
            }).await {
                let top = DailyTop {
                    message_id: message.id.0.to_string(),
                    channel_id: message.channel_id.0.to_string(),
                    top_type: ty.clone()
                };

                add_daily_top(data.db.clone(), &top).await;
            }

            vec![]
        } else {
            vec![
                EmbedData::new()
                    .title(trn!(data.lang, "error.dailytop.wrong_type.title"))
                    .desc(trn!(data.lang, "error.dailytop.wrong_type.desc", types.keys().map(|x| x.to_string()).collect::<Vec<String>>().join(", ")))
            ]
        }
    }
}