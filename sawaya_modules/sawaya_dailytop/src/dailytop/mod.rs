pub mod create;
pub mod delete;
pub mod update;

use sawaya_core::async_trait;
use sawaya_core::command::{BotCommand, no_run};
use sawaya_core::command::info::CommandInfo;
use sawaya_core::command::data::CommandData;
use sawaya_core::embed::EmbedData;
use crate::dailytop::create::DailyTopCreateCommand;
use crate::dailytop::delete::DailyTopDeleteCommand;
use crate::dailytop::update::DailyTopUpdateCommand;

pub struct DailyTopCommand;

#[async_trait]
impl BotCommand for DailyTopCommand {
    fn info(&self) -> CommandInfo {
        CommandInfo::new(
            "dailytop",
            "dailytop"
        ).children(
            vec![
                Box::new(DailyTopCreateCommand),
                Box::new(DailyTopDeleteCommand),
                Box::new(DailyTopUpdateCommand)
            ]
        )
    }

    async fn run(&self, data: CommandData) -> Vec<EmbedData> {
        no_run(data)
    }
}