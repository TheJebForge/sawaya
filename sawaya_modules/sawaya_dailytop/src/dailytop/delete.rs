use sawaya_core::async_trait;
use sawaya_core::trn;
use sawaya_core::command::BotCommand;
use sawaya_core::command::info::CommandInfo;
use sawaya_core::command::data::CommandData;
use sawaya_core::embed::EmbedData;
use sawaya_core::permissions::BotPermission;
use crate::db::remove_daily_tops_by_channel;

pub struct DailyTopDeleteCommand;

#[async_trait]
impl BotCommand for DailyTopDeleteCommand {
    fn info(&self) -> CommandInfo {
        CommandInfo::new(
            "dailytop-delete",
            "delete"
        ).permission(
            BotPermission::MANAGERS
        )
    }

    async fn run(&self, data: CommandData) -> Vec<EmbedData> {
        remove_daily_tops_by_channel(data.ctx.clone(), data.db.clone(), data.lang.clone(), &data.message.channel_id.0.to_string()).await;

        vec![
            EmbedData::new()
                .title(trn!(data.lang, "dailytop.delete.title"))
                .desc(trn!(data.lang, "dailytop.delete.desc"))
        ]
    }
}