use sawaya_core::async_trait;
use sawaya_core::trn;
use sawaya_core::command::BotCommand;
use sawaya_core::command::info::CommandInfo;
use sawaya_core::command::data::CommandData;
use sawaya_core::embed::EmbedData;
use crate::task::update_dailytops;
use sawaya_core::vp::VoiceProfileClient;

pub struct DailyTopUpdateCommand;

#[async_trait]
impl BotCommand for DailyTopUpdateCommand {
    fn info(&self) -> CommandInfo {
        CommandInfo::new(
            "dailytop-update",
                "update"
        )
    }

    async fn run(&self, data: CommandData) -> Vec<EmbedData> {
        update_dailytops(data.ctx.clone(), data.db.clone(), VoiceProfileClient::init(data.sensitive.clone())).await;

        vec![
            EmbedData::new()
                .title(trn!(data.lang, "dailytop.updated.title"))
                .desc(trn!(data.lang, "dailytop.updated.desc"))
        ]
    }
}