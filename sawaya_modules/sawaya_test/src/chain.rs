use sawaya_core::async_trait;
use sawaya_core::command::BotCommand;
use sawaya_core::command::data::CommandData;
use sawaya_core::command::info::CommandInfo;
use sawaya_core::embed::EmbedData;
use sawaya_core::command::chain::{ChainCommand, ChainCommandInfo, process_chain};
use sawaya_core::command::parameters::{Parameter, ParameterType};

pub struct ChainCommandTest;

#[async_trait]
impl BotCommand for ChainCommandTest {
    fn info(&self) -> CommandInfo {
        CommandInfo::new(
            "chain-command",
            "chain"
        )
    }

    async fn run(&self, data: CommandData) -> Vec<EmbedData> {
        let result = process_chain(
            MyData { a: "initial,".to_string() },
            vec![
                Box::new(ChainA),
                Box::new(ChainB)
            ],
            data.clone()
        ).await;

        match result {
            Ok(data) => {
                vec![
                    EmbedData::new()
                        .title("processed".to_string())
                        .desc(format!("{} done", data.a))
                ]
            }

            Err(err) => vec![err]
        }
    }
}



struct MyData {
    pub a: String
}

pub struct ChainA;

#[async_trait]
impl ChainCommand<MyData> for ChainA {
    fn info(&self) -> ChainCommandInfo {
        ChainCommandInfo::new(
            "chain-a",
            "a"
        ).parameters(
            &[
                Parameter::Required(ParameterType::String)
            ]
        )
    }

    async fn run(&self, shared: &mut MyData, data: CommandData) -> Result<(), EmbedData> {
        let str = &data.args[0].to_string();

        shared.a.push_str(&format!(" a: {},", str));

        Ok(())
    }
}

pub struct ChainB;

#[async_trait]
impl ChainCommand<MyData> for ChainB {
    fn info(&self) -> ChainCommandInfo {
        ChainCommandInfo::new(
            "chain-b",
            "b"
        ).parameters(
            &[
                Parameter::Required(ParameterType::String)
            ]
        )
    }

    async fn run(&self, shared: &mut MyData, data: CommandData) -> Result<(), EmbedData> {
        let str = &data.args[0].to_string();

        shared.a.push_str(&format!(" b: {},", str));

        Ok(())
    }
}