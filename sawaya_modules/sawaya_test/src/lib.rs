mod test;
mod subtest;
mod chain;

use sawaya_core::module::{BotModule, module_about_embed};
use sawaya_core::command::BotCommand;
use crate::test::TestCommand;
use std::sync::Arc;
use sawaya_core::lang::Lang;
use sawaya_core::embed::EmbedData;
use crate::chain::ChainCommandTest;
use sawaya_core::async_trait;

struct TestModule;

#[async_trait]
impl BotModule for TestModule {
    fn about(&self, lang: Arc<Lang>) -> EmbedData {
        module_about_embed(lang, self.codename())
            .color(0xb5b9ff.into())
    }

    fn codename(&self) -> &str {
        "test_module"
    }

    fn commands(&self) -> Vec<Box<dyn BotCommand + Sync + Send>> {
        vec![
            Box::new(TestCommand),
            Box::new(ChainCommandTest)
        ]
    }
}

pub fn init() {
    sawaya_core::add_boxed_module(Box::new(TestModule));
}