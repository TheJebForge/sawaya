use sawaya_core::serenity::async_trait;
use sawaya_core::command::{BotCommand, no_run};
use sawaya_core::command::data::CommandData;
use sawaya_core::embed::EmbedData;
use sawaya_core::command::info::CommandInfo;
use crate::subtest::SubTest;
use sawaya_core::permissions::BotPermission;

pub struct TestCommand;

#[async_trait]
impl BotCommand for TestCommand {
    fn info(&self) -> CommandInfo {
        CommandInfo::new(
            "test",
            "test"
        ).children(
            vec![
                Box::new(SubTest)
            ]
        ).permission(BotPermission::CREATOR)
    }

    async fn run(&self, data: CommandData) -> Vec<EmbedData> {
        no_run(data)
    }
}