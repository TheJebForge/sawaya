use sawaya_core::command::{BotCommand};
use sawaya_core::command::parameters::{Parameter, ParameterType};
use sawaya_core::serenity::async_trait;
use sawaya_core::command::data::CommandData;
use std::ops::Deref;
use sawaya_core::embed::EmbedData;
use sawaya_core::command::info::CommandInfo;

pub struct SubTest;

#[async_trait]
impl BotCommand for SubTest {
    fn info(&self) -> CommandInfo {
        CommandInfo::new(
            "test-subtest",
            "subtest"
        ).parameters(
            &[
                Parameter::Optional(ParameterType::String)
            ]
        )
    }

    async fn run(&self, data: CommandData) -> Vec<EmbedData> {
        data.message.reply(data.ctx.deref(), format!("i also have {:?}", data.args)).await.unwrap(); vec![]
    }
}