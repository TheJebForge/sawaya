use sawaya_core::trn;
use sawaya_core::async_trait;
use sawaya_core::command::BotCommand;
use sawaya_core::command::chain::process_chain;
use sawaya_core::command::data::CommandData;
use sawaya_core::command::info::CommandInfo;
use sawaya_core::command::parameters::{Parameter, ParameterType};
use sawaya_core::embed::EmbedData;
use sawaya_core::permissions::BotPermission;
use crate::commands::create::{CustomCreateCode, CustomCreateParam, CustomCreatePerm, CustomCreateScope};
use crate::CUSTOM_COMMAND_CACHE;
use crate::db::{update_custom_command};

pub struct CustomEditCommand;

#[async_trait]
impl BotCommand for CustomEditCommand {
    fn info(&self) -> CommandInfo {
        CommandInfo::new(
            "custom-edit",
            "edit"
        ).permission(
            BotPermission::CREATOR
        ).parameters(
            &[
                Parameter::Required(ParameterType::String)
            ]
        )
    }

    async fn run(&self, data: CommandData) -> Vec<EmbedData> {
        let name = data.args[0].to_string().trim().to_string();

        for def in unsafe { CUSTOM_COMMAND_CACHE.clone() } {
            if def.name == name.trim() {
                let def = process_chain(def, vec![
                    Box::new(CustomCreateScope),
                    Box::new(CustomCreatePerm),
                    Box::new(CustomCreateParam),
                    Box::new(CustomCreateCode)
                ], data.clone()).await;

                return match def {
                    Ok(def) => {
                        if def.code.is_empty() {
                            vec![
                                EmbedData::new()
                                    .title(trn!(data.lang, "error.custom_commands.code_isnt_specified.title"))
                                    .desc(trn!(data.lang, "error.custom_commands.code_isnt_specified.desc"))
                                    .failed()
                            ]
                        } else {
                            update_custom_command(data.db.clone(), def).await;

                            vec![
                                EmbedData::new()
                                    .title(trn!(data.lang, "custom_commands.updated.title"))
                                    .desc(trn!(data.lang, "custom_commands.updated.desc"))
                            ]
                        }
                    }

                    Err(embed) => vec![embed],
                }
            };
        }

        vec![
            EmbedData::new()
                .title(trn!(data.lang, "error.custom_commands.not_found.title"))
                .desc(trn!(data.lang, "error.custom_commands.not_found.desc"))
                .failed()
        ]
    }
}