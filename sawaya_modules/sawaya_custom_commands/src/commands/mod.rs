use sawaya_core::command::{BotCommand, no_run};
use sawaya_core::command::data::CommandData;
use sawaya_core::command::info::CommandInfo;
use sawaya_core::embed::EmbedData;
use sawaya_core::async_trait;
use sawaya_core::permissions::BotPermission;
use crate::commands::create::CustomCreateCommand;
use crate::commands::delete::CustomDeleteCommand;
use crate::commands::edit::CustomEditCommand;
use crate::commands::list::CustomListCommand;
use crate::commands::show::CustomShowCommand;

pub mod create;
pub mod edit;
pub mod show;
pub mod list;
pub mod delete;

pub struct CustomBaseCommand;

#[async_trait]
impl BotCommand for CustomBaseCommand {
    fn info(&self) -> CommandInfo {
        CommandInfo::new(
            "custom",
            "custom"
        ).permission(
            BotPermission::CREATOR
        ).children(
            vec![
                Box::new(CustomCreateCommand),
                Box::new(CustomEditCommand),
                Box::new(CustomShowCommand),
                Box::new(CustomListCommand),
                Box::new(CustomDeleteCommand),
            ]
        )
    }

    async fn run(&self, data: CommandData) -> Vec<EmbedData> {
        no_run(data)
    }
}