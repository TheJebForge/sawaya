use sawaya_core::async_trait;
use sawaya_core::command::BotCommand;
use sawaya_core::command::data::CommandData;
use sawaya_core::command::info::CommandInfo;
use sawaya_core::command::parameters::{Parameter, ParameterType};
use sawaya_core::embed::EmbedData;
use sawaya_core::permissions::BotPermission;
use sawaya_core::trn;
use crate::CUSTOM_COMMAND_CACHE;

pub struct CustomShowCommand;

#[async_trait]
impl BotCommand for CustomShowCommand {
    fn info(&self) -> CommandInfo {
        CommandInfo::new(
            "custom-show",
            "show"
        ).permission(
            BotPermission::CREATOR
        ).parameters(
            &[
                Parameter::Required(ParameterType::String)
            ]
        )
    }

    async fn run(&self, data: CommandData) -> Vec<EmbedData> {
        let name = data.args[0].to_string().trim().to_string();

        for def in unsafe { CUSTOM_COMMAND_CACHE.clone() } {
            if def.name == name.trim() {
                let mut embed = EmbedData::new()
                    .title(trn!(data.lang, "custom_commands.show.title", def.name));

                embed.add_field(&trn!(data.lang, "custom_commands.show.scope"), &format!("{}", def.scope), true);
                embed.add_field(&trn!(data.lang, "custom_commands.show.perm"), &format!("{}", def.permission), true);
                embed.add_field(&trn!(data.lang, "custom_commands.show.params"), &format!("{:?}", def.parameters), true);
                embed.add_field(&trn!(data.lang, "custom_commands.show.code"), &format!("```lua\n{}\n```", def.code.trim()), false);

                return vec![embed];
            }
        }

        vec![
            EmbedData::new()
                .title(trn!(data.lang, "error.custom_commands.not_found.title"))
                .desc(trn!(data.lang, "error.custom_commands.not_found.desc"))
                .failed()
        ]
    }
}