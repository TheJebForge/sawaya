use sawaya_core::async_trait;
use sawaya_core::command::BotCommand;
use sawaya_core::command::data::CommandData;
use sawaya_core::command::info::CommandInfo;
use sawaya_core::embed::EmbedData;
use sawaya_core::permissions::BotPermission;
use sawaya_core::trn;
use crate::CUSTOM_COMMAND_CACHE;

pub struct CustomListCommand;

#[async_trait]
impl BotCommand for CustomListCommand {
    fn info(&self) -> CommandInfo {
        CommandInfo::new(
            "custom-list",
            "list"
        ).permission(
            BotPermission::CREATOR
        )
    }

    async fn run(&self, data: CommandData) -> Vec<EmbedData> {
        let mut list = "".to_string();

        for def in unsafe { CUSTOM_COMMAND_CACHE.clone() } {
            list.push_str(&format!("{} (Scope: {})\n", def.name, def.scope.to_string()));
        }

        vec![
            EmbedData::new()
                .title(trn!(data.lang, "custom_commands.list.title"))
                .desc(list.trim().to_string())
        ]
    }
}