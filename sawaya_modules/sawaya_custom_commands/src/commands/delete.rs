use sawaya_core::trn;
use sawaya_core::async_trait;
use sawaya_core::command::BotCommand;
use sawaya_core::command::data::CommandData;
use sawaya_core::command::info::CommandInfo;
use sawaya_core::command::parameters::{Parameter, ParameterType};
use sawaya_core::embed::EmbedData;
use sawaya_core::permissions::BotPermission;
use crate::CUSTOM_COMMAND_CACHE;
use crate::db::remove_custom_command;

pub struct CustomDeleteCommand;

#[async_trait]
impl BotCommand for CustomDeleteCommand {
    fn info(&self) -> CommandInfo {
        CommandInfo::new(
            "custom-delete",
            "delete"
        ).permission(
            BotPermission::CREATOR
        ).parameters(
            &[
                Parameter::Required(ParameterType::String)
            ]
        )
    }

    async fn run(&self, data: CommandData) -> Vec<EmbedData> {
        let name = data.args[0].to_string().trim().to_string();

        for def in unsafe { CUSTOM_COMMAND_CACHE.clone() } {
            if def.name == name.trim() {
                remove_custom_command(data.db.clone(), &def.name).await;
                return vec![
                    EmbedData::new()
                        .title(trn!(data.lang, "custom_commands.deleted.title"))
                        .desc(trn!(data.lang, "custom_commands.deleted.desc"))
                ];
            }
        }

        vec![
            EmbedData::new()
                .title(trn!(data.lang, "error.custom_commands.not_found.title"))
                .desc(trn!(data.lang, "error.custom_commands.not_found.desc"))
                .failed()
        ]
    }
}