use std::collections::HashMap;
use sawaya_core::command::BotCommand;
use sawaya_core::command::data::CommandData;
use sawaya_core::command::info::CommandInfo;
use sawaya_core::embed::EmbedData;
use sawaya_core::{async_trait, get_module_commands};
use sawaya_core::trn;
use sawaya_core::command::chain::{ChainCommand, ChainCommandInfo, process_chain};
use sawaya_core::command::parameters::{Parameter, ParameterType};
use sawaya_core::permissions::BotPermission;
use crate::custom::{CustomCommandDefinition, CustomCommandScope};
use crate::db::add_custom_command;

pub struct CustomCreateCommand;

#[async_trait]
impl BotCommand for CustomCreateCommand {
    fn info(&self) -> CommandInfo {
        CommandInfo::new(
            "custom-create",
            "create"
        ).permission(
            BotPermission::CREATOR
        ).parameters(
            &[
                Parameter::Required(ParameterType::String)
            ]
        )
    }

    async fn run(&self, data: CommandData) -> Vec<EmbedData> {
        let name = data.args[0].to_string().trim().to_string();

        let def = process_chain(CustomCommandDefinition {
            name,
            scope: CustomCommandScope::Application,
            code: "".to_string(),
            permission: BotPermission::ANYONE,
            parameters: vec![]
        }, vec![
            Box::new(CustomCreateScope),
            Box::new(CustomCreatePerm),
            Box::new(CustomCreateParam),
            Box::new(CustomCreateCode)
        ], data.clone()).await;

        match def {
            Ok(def) => {
                if check_if_command_exists(&def.name) {
                    vec![
                        EmbedData::new()
                            .title(trn!(data.lang, "error.custom_commands.name_already_taken.title"))
                            .desc(trn!(data.lang, "error.custom_commands.name_already_taken.desc"))
                            .failed()
                    ]
                } else {
                    if def.code.is_empty() {
                        vec![
                            EmbedData::new()
                                .title(trn!(data.lang, "error.custom_commands.code_isnt_specified.title"))
                                .desc(trn!(data.lang, "error.custom_commands.code_isnt_specified.desc"))
                                .failed()
                        ]
                    } else {
                        add_custom_command(data.db.clone(), def).await;

                        vec![
                            EmbedData::new()
                                .title(trn!(data.lang, "custom_commands.created.title"))
                                .desc(trn!(data.lang, "custom_commands.created.desc"))
                        ]
                    }
                }
            }

            Err(embed) => vec![embed],
        }
    }
}

fn check_if_command_exists(name: &str) -> bool {
    let commands = get_module_commands();

    for command_container in commands.values() {
        for command in command_container {
            if command.info().name == name {
                return true;
            }
        }
    }

    return false;
}



pub struct CustomCreateScope;

#[async_trait]
impl ChainCommand<CustomCommandDefinition> for CustomCreateScope {
    fn info(&self) -> ChainCommandInfo {
        ChainCommandInfo::new(
            "custom-create-scope",
            "scope"
        ).parameters(
            &[
                Parameter::Required(ParameterType::String)
            ]
        )
    }

    async fn run(&self, shared: &mut CustomCommandDefinition, data: CommandData) -> Result<(), EmbedData> {
        let mut scopes = HashMap::new();
        scopes.insert("app".to_string(), CustomCommandScope::Application);
        scopes.insert("guild".to_string(), CustomCommandScope::Guild(data.guild_id.parse().unwrap()));

        let scope = data.args[0].to_string();

        if let Some(scope) = scopes.get(&scope) {
            shared.scope = scope.clone();
            Ok(())
        } else {
            let scope_names = scopes.keys().map(|x| x.to_string()).collect::<Vec<String>>().join(", ");
            Err(
                EmbedData::new()
                    .title(trn!(data.lang, "error.custom_commands.wrong_scope.title"))
                    .desc(trn!(data.lang, "error.custom_commands.wrong_scope.desc", scope_names))
                    .failed()
            )
        }
    }
}

pub struct CustomCreatePerm;

#[async_trait]
impl ChainCommand<CustomCommandDefinition> for CustomCreatePerm {
    fn info(&self) -> ChainCommandInfo {
        ChainCommandInfo::new(
            "custom-create-perm",
            "perm"
        ).parameters(
            &[
                Parameter::Required(ParameterType::String)
            ]
        )
    }

    async fn run(&self, shared: &mut CustomCommandDefinition, data: CommandData) -> Result<(), EmbedData> {
        let mut perms = HashMap::new();
        perms.insert("anyone".to_string(), BotPermission::ANYONE);
        perms.insert("managers".to_string(), BotPermission::MANAGERS);
        perms.insert("creator".to_string(), BotPermission::CREATOR);

        let perm = data.args[0].to_string();

        if let Some(perm) = perms.get(&perm) {
            shared.permission = perm.clone();
            Ok(())
        } else {
            let perm_names = perms.keys().map(|x| x.to_string()).collect::<Vec<String>>().join(", ");
            Err(
                EmbedData::new()
                    .title(trn!(data.lang, "error.custom_commands.wrong_permission.title"))
                    .desc(trn!(data.lang, "error.custom_commands.wrong_permission.desc", perm_names))
                    .failed()
            )
        }
    }
}

pub struct CustomCreateParam;

#[async_trait]
impl ChainCommand<CustomCommandDefinition> for CustomCreateParam {
    fn info(&self) -> ChainCommandInfo {
        ChainCommandInfo::new(
            "custom-create-param",
            "param"
        ).parameters(
            &[
                Parameter::Required(ParameterType::String),
                Parameter::Required(ParameterType::String),
            ]
        )
    }

    async fn run(&self, shared: &mut CustomCommandDefinition, data: CommandData) -> Result<(), EmbedData> {
        let mut param_types = HashMap::new();
        param_types.insert("integer".to_string(), ParameterType::Integer);
        param_types.insert("float".to_string(), ParameterType::Float);
        param_types.insert("string".to_string(), ParameterType::String);
        param_types.insert("text".to_string(), ParameterType::Text);
        param_types.insert("mention".to_string(), ParameterType::Mention);

        let param_type = data.args[1].to_string();

        if let Some(param_type) = param_types.get(&param_type) {
            let mut param_requirements = HashMap::new();
            param_requirements.insert("required".to_string(), Parameter::Required(param_type.clone()));
            param_requirements.insert("optional".to_string(), Parameter::Optional(param_type.clone()));

            let param_requirement = data.args[0].to_string();

            if let Some(param_requirement) = param_requirements.get(&param_requirement) {
                shared.parameters.push(param_requirement.clone());
                Ok(())
            } else {
                let param_names = param_requirements.keys().map(|x| x.to_string()).collect::<Vec<String>>().join(", ");
                Err(
                    EmbedData::new()
                        .title(trn!(data.lang, "error.custom_commands.wrong_param_requirement.title"))
                        .desc(trn!(data.lang, "error.custom_commands.wrong_param_requirement.desc", param_names))
                        .failed()
                )
            }
        } else {
            let param_names = param_types.keys().map(|x| x.to_string()).collect::<Vec<String>>().join(", ");
            Err(
                EmbedData::new()
                    .title(trn!(data.lang, "error.custom_commands.wrong_param_type.title"))
                    .desc(trn!(data.lang, "error.custom_commands.wrong_param_type.desc", param_names))
                    .failed()
            )
        }
    }
}

pub struct CustomCreateCode;

#[async_trait]
impl ChainCommand<CustomCommandDefinition> for CustomCreateCode {
    fn info(&self) -> ChainCommandInfo {
        ChainCommandInfo::new(
            "custom-create-code",
            "code"
        ).parameters(
            &[
                Parameter::Required(ParameterType::Text),
            ]
        )
    }

    async fn run(&self, shared: &mut CustomCommandDefinition, data: CommandData) -> Result<(), EmbedData> {
        let code = data.args[0].to_text();
        shared.code = code;
        Ok(())
    }
}