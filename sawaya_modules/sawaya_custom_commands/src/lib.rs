mod custom;
mod db;
mod commands;

use std::sync::Arc;
use sawaya_core::command::BoxedCommand;
use sawaya_core::embed::EmbedData;
use sawaya_core::lang::Lang;
use sawaya_core::module::{BotModule, module_about_embed};
use crate::custom::CustomCommandDefinition;
use sawaya_core::async_trait;
use sawaya_core::db::DB;
use crate::commands::CustomBaseCommand;
use crate::custom::template::CustomCommand;
use crate::db::get_custom_command_definitions;

static mut CUSTOM_COMMAND_CACHE: Vec<CustomCommandDefinition> = vec![];

pub(crate) fn register_custom_command(definition: CustomCommandDefinition) {
    unsafe {
        CUSTOM_COMMAND_CACHE.push(definition)
    }
}

pub(crate) fn get_custom_commands() -> Vec<BoxedCommand> {
    let cache = unsafe { &CUSTOM_COMMAND_CACHE };
    let mut commands: Vec<BoxedCommand> = vec![];

    for definition in cache {
        commands.push(Box::new(CustomCommand {
            definition: definition.clone()
        }))
    }

    commands
}

pub(crate) async fn refresh_custom_command_definitions(db: Arc<DB>) {
    let definitions = get_custom_command_definitions(db.clone()).await;

    unsafe {
        CUSTOM_COMMAND_CACHE.clear()
    }

    for definition in definitions {
        register_custom_command(definition);
    }
}















struct CustomCommandsModule;

#[async_trait]
impl BotModule for CustomCommandsModule {
    fn about(&self, lang: Arc<Lang>) -> EmbedData {
        module_about_embed(lang, self.codename())
            .color(0xb28dff.into())
    }

    fn codename(&self) -> &str {
        "custom_commands"
    }

    fn commands(&self) -> Vec<BoxedCommand> {
        let mut commands: Vec<BoxedCommand> = vec![
            Box::new(CustomBaseCommand)
        ];

        commands.extend(get_custom_commands().into_iter());
        commands
    }
}

pub async fn init(db: Arc<DB>) {
    refresh_custom_command_definitions(db.clone()).await;
    sawaya_core::add_boxed_module(Box::new(CustomCommandsModule));
}