use std::sync::Arc;
use sawaya_core::db::{DB, process_result};
use crate::custom::CustomCommandDefinition;
use crate::refresh_custom_command_definitions;

pub async fn get_custom_command_definitions(db: Arc<DB>) -> Vec<CustomCommandDefinition> {
    let rows = db.0.query("select * from bot.custom_commands", &[]).await;

    let mut definitions = vec![];

    match rows {
        Ok(rows) => {
            for row in rows {
                definitions.push(CustomCommandDefinition::from(&row));
            }
        }

        Err(error) => {
            log::error!("Failed to get custom commands: {:?}", error);
        }
    }

    definitions
}

pub async fn add_custom_command(db: Arc<DB>, definition: CustomCommandDefinition) {
    let scope = serde_json::to_string(&definition.scope).unwrap();
    let permission = serde_json::to_string(&definition.permission).unwrap();
    let parameters = serde_json::to_string(&definition.parameters).unwrap();
    process_result(db.0.execute("insert into bot.custom_commands (name, scope, code, permission, parameters) values \
            ($1, $2, $3, $4, $5);", &[&definition.name, &scope, &definition.code, &permission, &parameters]).await);

    refresh_custom_command_definitions(db.clone()).await;
}

pub async fn update_custom_command(db: Arc<DB>, definition: CustomCommandDefinition) {
    let scope = serde_json::to_string(&definition.scope).unwrap();
    let permission = serde_json::to_string(&definition.permission).unwrap();
    let parameters = serde_json::to_string(&definition.parameters).unwrap();

    process_result(db.0.execute("UPDATE bot.custom_commands
	SET scope=$2, code=$3, permission=$4, parameters=$5
	WHERE name = $1;", &[&definition.name, &scope, &definition.code, &permission, &parameters]).await);

    refresh_custom_command_definitions(db.clone()).await;
}

pub async fn remove_custom_command(db: Arc<DB>, name: &str) {
    process_result(db.0.execute("delete from bot.custom_commands \
            where name = $1;", &[&name]).await);

    refresh_custom_command_definitions(db.clone()).await;
}