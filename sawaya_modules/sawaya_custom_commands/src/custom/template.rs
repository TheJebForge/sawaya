use std::ops::Deref;
use std::sync::Arc;
use sawaya_core::command::BotCommand;
use crate::custom::{CustomCommandDefinition, CustomCommandScope};
use sawaya_core::async_trait;
use sawaya_core::command::data::CommandData;
use sawaya_core::command::info::CommandInfo;
use sawaya_core::embed::EmbedData;
use sawaya_core::lua::bot::argument::LuaArgument;
use sawaya_core::lua::create_lua_state;
use sawaya_core::serenity::model::id::GuildId;
use sawaya_core::trn;

pub struct CustomCommand {
    pub definition: CustomCommandDefinition
}

#[async_trait]
impl BotCommand for CustomCommand {
    fn info(&self) -> CommandInfo {
        let mut info = CommandInfo::new(
            &self.definition.name,
            &self.definition.name
        ).permission(
            self.definition.permission
        );

        info.parameters = self.definition.parameters.clone();

        info
    }

    async fn run(&self, data: CommandData) -> Vec<EmbedData> {
        if let CustomCommandScope::Guild(id) = self.definition.scope {
            if id.to_string() != data.guild_id {
                if let Some(guild) = GuildId(id).to_guild_cached(data.ctx.deref()).await {
                    return vec![
                        EmbedData::new()
                            .title(trn!(data.lang, "error.custom_commands.not_intended_guild.title"))
                            .desc(trn!(data.lang, "error.custom_commands.not_intended_guild.desc", guild.name))
                            .failed()
                    ]
                }
            }
        }

        let data = Arc::new(data);
        let arguments: Vec<LuaArgument> = data.args.iter().map(|x| LuaArgument::from_argument(data.ctx.clone(), x.clone())).collect();

        let lua = create_lua_state(data.clone());

        lua.lua.globals().set("args", arguments).unwrap();

        let result = lua.execute(self.definition.code.to_string());

        match result {
            Ok(_) => {
                vec![]
            }
            Err(err) => {
                vec![
                    EmbedData::new()
                        .title("Execution failed".to_string())
                        .desc(err.to_string())
                        .failed()
                ]
            }
        }
    }
}