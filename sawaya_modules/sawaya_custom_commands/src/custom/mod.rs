pub mod template;

use std::fmt::{Display, Formatter};
use serde::{Serialize, Deserialize};
use tokio_postgres::Row;
use sawaya_core::command::parameters::Parameter;
use sawaya_core::permissions::BotPermission;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct CustomCommandDefinition {
    pub name: String,
    pub scope: CustomCommandScope,
    pub code: String,
    pub permission: BotPermission,
    pub parameters: Vec<Parameter>,
}

impl From<&Row> for CustomCommandDefinition {
    fn from(row: &Row) -> Self {
        let scope_str: String = row.get("scope");
        let scope = serde_json::from_str(&scope_str).unwrap();

        let permission_str: String = row.get("permission");
        let permission = serde_json::from_str(&permission_str).unwrap();

        let parameters_str: String = row.get("parameters");
        let parameters = serde_json::from_str(&parameters_str).unwrap();

        Self {
            name: row.get("name"),
            scope,
            code: row.get("code"),
            permission,
            parameters,
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Copy, Debug)]
pub enum CustomCommandScope {
    Application,
    Guild(u64)
}

impl Display for CustomCommandScope {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", match self {
            CustomCommandScope::Application => format!("Application"),
            CustomCommandScope::Guild(id) => format!("Guild {}", id),
        })
    }
}